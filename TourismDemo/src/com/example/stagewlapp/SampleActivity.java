package com.example.stagewlapp;

import wazzatimagescanner.WLAuthenticate;
import wazzatimagescanner.WLAuthenticateListener;
import wazzatimagescanner.WLAuthenticationException;
import wazzatimagescanner.WLClientAcitivityInfo;
import wazzatimagescanner.WLData;
import wazzatimagescanner.WLSyncManager;
import wazzatimagescanner.WLSyncStatusListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

public class SampleActivity extends Activity implements WLAuthenticateListener,WLSyncStatusListener{

	
	ProgressBar progressBar = null;
	ProgressDialog pd ;

	@SuppressLint("ServiceCast")
	@Override	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.samplea_layout_activity);
		
		progressBar = (ProgressBar)findViewById(R.id.pb);		
		progressBar.setProgress(0);
		progressBar.setMax(130);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		WLClientAcitivityInfo.setContext(this);
		
		WLClientAcitivityInfo.setUseGV();
				
		WLAuthenticate.Authenticate("34cb9442a183f19cf9a0",this);		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.sample, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onAuthenticationSuccess() {
		Log.i("sample activity","This is called at last");
		if( WLAuthenticate.alreadyAuthenticated()){
			progressBar.setProgress(130);

			if(WLAuthenticate.isUserAuthenticated()){
				try {
					if( WLData.getObjectList().size() == 0 ){
						Toast.makeText(this, "Database empty. Add an object by clicking its photo. ", Toast.LENGTH_LONG).show();
					}else{
						Toast.makeText(this, "You have "+ Integer.toString( WLData.getObjectList().size() ) + " objects in database. Start Wazzat-ing!" , Toast.LENGTH_LONG).show();
					}
				} catch (WLAuthenticationException e) {
					e.printStackTrace();
				}				
			}		
			Intent i =  new Intent(SampleActivity.this,MainActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
		}else{ 			
			WLSyncManager syncManger = new WLSyncManager(WLSyncManager.SYNC_MODE.DOWNLOAD_ONLY, this);
			syncManger.sync();
		}
		
	}

	@Override
	public void whileAuthenticating() {
		//pb.setProgress();
	}

	@Override
	public ProgressBar getProgressBar() {
		return progressBar;
	}

	@Override
	public void onAuthenticationFailure(int arg0) {
		
		if(arg0 == WLAuthenticate.DATA_LOAD_FAILED || arg0 == WLAuthenticate.DOWNLOAD_FAILED){
			Toast.makeText(this, "Authorization Failed for the given token. Check you internet connection", Toast.LENGTH_LONG).show();
		}
		else if(arg0 == WLAuthenticate.TOKEN_EXPIRED){
			Toast.makeText(this, "Token has been expired. Please connect to internet and try again",Toast.LENGTH_LONG).show();
		}
		Intent i =  new Intent(SampleActivity.this,MainActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);		
	}

	@Override
	public void onSyncFailure(int arg0) {
		if(pd != null ){
			pd.dismiss();
		}
		progressBar.setProgress(130);
		if(WLAuthenticate.isUserAuthenticated()){
			try {
				if( WLData.getObjectList().size() == 0 ){
					Toast.makeText(this, "Database empty. Add an object by clicking its photo. ", Toast.LENGTH_LONG).show();
				}else{
					Toast.makeText(this, "You have "+ Integer.toString( WLData.getObjectList().size() ) + " objects in database. Start Wazzat-ing!" , Toast.LENGTH_LONG).show();
				}
			} catch (WLAuthenticationException e) {
				e.printStackTrace();
			}
			
		}		
		Intent i =  new Intent(SampleActivity.this,MainActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
	}

	@Override
	public void onSyncSuccess() {
		if(pd != null ){
			pd.dismiss();
		}
		progressBar.setProgress(130);
		if(WLAuthenticate.isUserAuthenticated()){
			try {
				if( WLData.getObjectList().size() == 0 ){
					Toast.makeText(this, "Database empty. Add an object by clicking its photo. ", Toast.LENGTH_LONG).show();
				}else{
					Toast.makeText(this, "You have "+ Integer.toString( WLData.getObjectList().size() ) + " objects in database. Start Wazzat-ing!" , Toast.LENGTH_LONG).show();
				}
			} catch (WLAuthenticationException e) {
				e.printStackTrace();
			}
			
		}		
		Intent i =  new Intent(SampleActivity.this,MainActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
	}

	@Override
	public void preSyncTask() {
		progressBar.setProgress(110);
		//Toast.makeText(this, "Downloading inititial data", Toast.LENGTH_LONG).show();
		/*Log.i("Main Activity","Show progress Dialog Bar");

		pd = new ProgressDialog(this);
		pd.setTitle("Initializing...");
		pd.setMessage("Please wait....");
		pd.setCancelable(false);
		pd.setIndeterminate(true);
		pd.show();		
		Log.i("Main Activity","Done");*/
		
	}

	@Override
	public void whileSyncing() {
		
	}
}