package com.example.stagewlapp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class FeedbackActivity extends Activity {

	
	RadioGroup r1,r2,r3;
	ProgressDialog pd;
	private Context context;


	EditText e1;
	
	static final String FEEDBACK_URL = "http://www.wazzatlabs.com/feedbackFile.php";

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		context = this;
		
		findViewById(R.id.feedbackBtn).setOnTouchListener(
				mDelayHideTouchListener);
		
		
		r1= (RadioGroup)findViewById(R.id.rad1);
		r2= (RadioGroup)findViewById(R.id.rad2);
		r3= (RadioGroup)findViewById(R.id.rad3);
		

		
		e1 = (EditText)findViewById(R.id.fd_text);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.feedback, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent) {
			
			StringBuilder feedbackData = new StringBuilder();
			feedbackData.append(((RadioButton)findViewById(r1.getCheckedRadioButtonId())).getText()).append("\n");
			feedbackData.append(((RadioButton)findViewById(r2.getCheckedRadioButtonId())).getText()).append("\n");
			feedbackData.append(((RadioButton)findViewById(r3.getCheckedRadioButtonId())).getText()).append("\n");			
		    feedbackData.append(e1.getText());
		    
			String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
			String applicationName="SampleWLApp";
			PackageManager lPackageManager = getBaseContext().getPackageManager();
			ApplicationInfo lApplicationInfo = null;
			try {
				lApplicationInfo = lPackageManager.getApplicationInfo(getBaseContext().getApplicationInfo().packageName, 0);
			} catch (final NameNotFoundException e) {
				applicationName="WazzatLabs";
			}
			applicationName= (String) (lApplicationInfo != null ? lPackageManager.getApplicationLabel(lApplicationInfo) : "Unknown");
		    String deviceId = Secure.getString(getBaseContext().getContentResolver(),Secure.ANDROID_ID);
		    String dir_name = externalStorage + File.separator + applicationName;
			final String feedback_file_name = "feedback_"+deviceId+"_"+"DEMO2.txt";
			final String taskApplicatioName = applicationName;		
			try {
				File dir = new File (dir_name);
				File file = new File(dir, feedback_file_name);
				FileOutputStream f = new FileOutputStream(file);
				f.write(feedbackData.toString().getBytes());
				f.close();				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}	
			
			if(isConnected()){
				new AsyncTask<String, String, String>() {
					boolean isSuccess = false;
						
					
					
					@Override
					protected String doInBackground(String... params) {
						isSuccess = doUpload(feedback_file_name,taskApplicatioName);
						return null;
					}
					protected void onPostExecute(String result) {
						if(isSuccess){
							Toast.makeText(getApplicationContext(), "Thanks a lot for the Feedback!", Toast.LENGTH_LONG).show();	

						}else{
							Toast.makeText(getApplicationContext(), "Can not upload feedback form. Please try again!", Toast.LENGTH_LONG).show();	

						}
						if (pd!=null) {
							pd.dismiss();							
						}
						Intent i = new Intent(FeedbackActivity.this,MainActivity.class);
						i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(i);
					};
					
					protected void onPreExecute() {
						pd = new ProgressDialog(context);
						pd.setTitle("Uploading...");
						pd.setMessage("Please wait.");
						pd.setCancelable(false);
						pd.setIndeterminate(true);
						pd.show();
					};
					
				}.execute();
			}
			else{
				Toast.makeText(getApplicationContext(), "Please switch on internet to send feedback", Toast.LENGTH_LONG).show();	
			}

			return false;
		}
	};
	
	public boolean isConnected(){
		ConnectivityManager cm =
				(ConnectivityManager)getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null &&
				activeNetwork.isConnectedOrConnecting();
		return isConnected;
	}
	
	private boolean doUpload(String file_name,String applicationName) {
		boolean ret = false;
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		String lineEnd = "\r\n";
		int maxBufferSize = 1 * 1024 * 1024;
		String twoHyphens = "--";
		String boundary = "*****";
		File sourceFile = new File(externalStorage+File.separator+applicationName+File.separator+file_name); 
		if (!sourceFile.isFile()) {                         
			Log.e("uploadFile", "Source File not exist :"+file_name );              
			return ret;

		}
		try{
			FileInputStream fileInputStream = new FileInputStream(sourceFile);
			URL url = new URL(FEEDBACK_URL);
			String uploadFileName = file_name;
			Log.i("Server Sync","File upload URL" + uploadFileName);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection(); 
			conn.setDoInput(true); 
			conn.setDoOutput(true);
			conn.setUseCaches(false);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
			conn.setRequestProperty("uploaded_file", uploadFileName); 

			Log.i("Upload_File", "Upload file: " + uploadFileName + " SourceFile: "+ sourceFile);
			
			DataOutputStream dos = new DataOutputStream(conn.getOutputStream());         

			dos.writeBytes(twoHyphens + boundary + lineEnd); 
            dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                                      + uploadFileName + "\"" + lineEnd);
             
            dos.writeBytes(lineEnd);
			// create a buffer of  maximum size
			bytesAvailable = fileInputStream.available(); 

			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			buffer = new byte[bufferSize];

			// read file and write it into form...
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);  

			while (bytesRead > 0) {

				dos.write(buffer, 0, bufferSize);
				bytesAvailable = fileInputStream.available();
				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);   

			}

			// send multipart form data necesssary after file data...
			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			int serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();
			Log.i("uploadFile", "HTTP Response is : "
					+ serverResponseMessage + ": " + serverResponseCode);

			if(serverResponseCode == 200){
				ret =true;
				InputStream in = new BufferedInputStream(conn.getInputStream());
				String result = "";			

				if(in != null){
					BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(in));
					String line = "";
					while((line = bufferedReader.readLine()) != null)
						result += line;

					in.close();				
				}
				Log.i("Result:shit","Result:"+result);
				Log.i("WLSync","Upload successfull for file : " +file_name);
			}    

			//close the streams //
			fileInputStream.close();
			dos.flush();
			dos.close();

		} catch (Exception e) {               
			e.printStackTrace();  
			Log.e("Upload file to server Exception", "Exception : "
					+ e.getMessage(), e);
			ret =false;
		}

		return ret;
	}

}
