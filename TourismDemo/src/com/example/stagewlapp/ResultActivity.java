package com.example.stagewlapp;

import wazzatimagescanner.WLData;
import wazzatimagescanner.facebook.FacebookActivity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ResultActivity extends Activity {

	static String RESULT = "com.example.stagewlapp.RESULT";
	static String CHILD = "com.example.stagewlapp.CHILD";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);

		if(WLData.getLastCaptureImage()!=null){

			Bitmap bmp = BitmapFactory.decodeByteArray(WLData.getLastCaptureImage(),0,WLData.getLastCaptureImage().length);
			if( bmp!=null){
				double aspectRatio = (double) bmp.getHeight()/bmp.getWidth();		
				int targetW =getWindowManager().getDefaultDisplay().getWidth(); 

				int targetH = (int) (targetW * aspectRatio);
				Bitmap targetBmp = Bitmap.createScaledBitmap(bmp, targetW, targetH, false);				
				ImageView iv = (ImageView) findViewById(R.id.result_image);
				iv.setImageBitmap(targetBmp);
			}
		}
		
		TextView result = (TextView) findViewById(R.id.result_header);

		final String resultStr = getIntent().getStringExtra(RESULT);
		result.setText(resultStr);
		
		ImageButton ib = (ImageButton) findViewById(R.id.fshare);
		ib.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				FacebookActivity.id_activity = R.layout.activity_facebook;
				FacebookActivity.id_login = R.id.fb_login_button;
				FacebookActivity.id_post = R.id.post_image;
				FacebookActivity.id_status = R.id.update_status;
				FacebookActivity.id_user_name = R.id.user_name;
				Intent i =  new Intent(ResultActivity.this, FacebookActivity.class);
				i.putExtra("MESSAGE", resultStr);
				startActivity(i);

			}
		});		

		String[] childs = getIntent().getStringArrayExtra(CHILD);
		if(childs != null && childs.length > 0 ){	
			LinearLayout cont = (LinearLayout)findViewById(R.id.childcont);
			TextView resultChild = new TextView(this);
			resultChild.setText(" Following places can be viewed from here");
			resultChild.setTextSize(TypedValue.COMPLEX_UNIT_SP, 23);
			cont.addView(resultChild);
			for(int i = 0; i < childs.length ; ++i){
				TextView temp =  new TextView(this);
				Log.i("Childs", childs[i]);								
				temp.setText(childs[i]);
				temp.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
				temp.setCompoundDrawablePadding(8);
				temp.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.ic_input_add, 0, 0, android.R.drawable.divider_horizontal_dark);
				cont.addView(temp);
			}
		}			
	}

	@Override
	protected void onResume() {	
		super.onResume();		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.result, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
