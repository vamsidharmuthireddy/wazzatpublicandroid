package com.example.stagewlapp;

import java.util.ArrayList;

import wazzatimagescanner.WLAuthenticationException;
import wazzatimagescanner.WLData;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MultipleResultActivity extends Activity {

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multiple_result);
		LinearLayout container = (LinearLayout)findViewById(R.id.resultCont);

		ArrayList<String> resultList = MainActivity.MULTIPLE_RESULT_STRING; 

		for(int i = 0; i < resultList.size() && !resultList.get(i).isEmpty(); ++i ){

			final String name = resultList.get(i);

			LinearLayout tempLL = new LinearLayout(this);
			tempLL.setPadding(0, 15, 0, 0);
			tempLL.setOrientation(LinearLayout.HORIZONTAL);

			TextView showList = (TextView)getLayoutInflater().inflate(R.layout.text_view_style, null);
			showList.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
			showList.setText(resultList.get(i));
			showList.setCompoundDrawablePadding(8);
			showList.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.ic_input_add, 0, 0, android.R.drawable.divider_horizontal_dark);


			ImageView thumbnail = new ImageView(this);


			if( resultList.get(i).equalsIgnoreCase("NULL")){
				showList.setText("None of the above");
				thumbnail.setBackgroundResource(android.R.drawable.ic_menu_report_image);	
			}else{
				BitmapDrawable bD =  new BitmapDrawable(getResources(), WLData.getThumbnail(resultList.get(i)));
				thumbnail.setBackgroundResource(android.R.drawable.ic_menu_report_image);
				if( Integer.valueOf(android.os.Build.VERSION.SDK_INT) >= 16){
					thumbnail.setBackground(bD);
				}
				else{		
					thumbnail.setBackgroundDrawable(bD);
				}		
			}
			final String clickableAnnotation =  resultList.get(i);
			tempLL.addView(thumbnail);
			tempLL.addView(showList);

			tempLL.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent i = new Intent(MultipleResultActivity.this, ResultActivity.class);
					i.putExtra(ResultActivity.RESULT, clickableAnnotation);
					try {
						ArrayList<String> childObjects =  WLData.getChildObjects(clickableAnnotation);
						if(childObjects != null){
							String[] temp = new String[childObjects.size()];
							temp = childObjects.toArray(temp);
							i.putExtra(ResultActivity.CHILD, temp);
						}
					} catch (WLAuthenticationException e) {
						e.printStackTrace();
					}					
					startActivity(i);														
				}
			});

			container.addView(tempLL);						
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.multiple_result, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
