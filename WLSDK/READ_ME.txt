-This is project is used only for creating libraries for android project.
- Content in jni folder is used for creating linux .so share libraries
- The java source file is used for creating .Jar file.

How to create .so file
1) Change the include path of the jni/Android.mk file to point to the OpencCv.mk file
2) Do ndk-build
3) Libraries will be placed in the libs/armeabi*

How to create .jar file
1) Build the java project(Compile the project)
2) Go to bin/classes (if classes is not present then bin/. Make sure wazzatcardscanner folder is present there)
3) jar -cvf <output_file_path> wazzatcardscanner/WL*
4) Go to the output path and grab the library

