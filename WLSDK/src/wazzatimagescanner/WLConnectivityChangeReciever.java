package wazzatimagescanner;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

public class WLConnectivityChangeReciever extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		if(isConnected(context)){		
			// now internet is up send the api call information to server
			StringBuilder url = new StringBuilder("http://www.wazzatlabs.com/updateApiCallCount.php");
			url.append("?")
			.append("token=")
			.append(WLSharedPreference.getInstance().getTokenID())
			.append("&")
			.append("count=")
			.append(WLSharedPreference.getInstance().getAPICallsCount());			
			
			new UpdateServerTask().execute(url.toString());
		}	
	}

	private boolean isConnected(Context context){

		ConnectivityManager cm =
				(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null &&
				activeNetwork.isConnectedOrConnecting();
		return isConnected;
	}

	private class UpdateServerTask extends AsyncTask<String, Void, Void> {
		
		@Override
		protected Void doInBackground(String... params) {

			String server_url = params[0];
			URL uri = null;
			HttpURLConnection urlConnection = null;
			try {
				uri = new URL(server_url);
				Log.i("Server Sync","server script URL:" + server_url);


				urlConnection = (HttpURLConnection) uri.openConnection();
				InputStream in = new BufferedInputStream(urlConnection.getInputStream());
				String result = "";			

				if(in != null){
					BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(in));
					String line = "";
					while((line = bufferedReader.readLine()) != null)
						result += line;

					in.close();				
				}
				Log.i("Upload Server Sync","Result:" + result);				

			} catch (MalformedURLException e) {

				e.printStackTrace();
			}
			catch (IOException e) {

				e.printStackTrace();	
			}finally{

				urlConnection.disconnect();
			}
			return null;
		}

	}

}
