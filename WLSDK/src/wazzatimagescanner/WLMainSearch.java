package wazzatimagescanner;

import wazzatimagescanner.Direction.DIRECTION;
import wazzatimagescanner.camera.WLCameraActivity;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.util.Log;

public class WLMainSearch {

	private static final String NO_RESULT = "NULL";
	
	private static boolean mIsCurrentDirectionApproxSame = false;
	
	private static boolean mIsCurrentDirectionExactSame = false;
	
	private static double mCurrentDistance  = -1;	
	
	private enum RANGE{		
		TOO_FAR(100,Double.MAX_VALUE),
		TOO_NEAR(0,10),
		CLOSE(10,50),
		FAR(50,100);		
		
		private final double mMin;
		private final double mMax;
		
		private RANGE(final double  min, final double max){
			this.mMin = min;
			this.mMax = max;
		}		
		
		boolean inRange(double x){
			return (mMin <= x && x < mMax);			
		}
	}
	
	private static Context mContext = null;
	
	private static Activity mActivity = null;
	
	private static Location mTargetLocation = null;
	
	public WLMainSearch(Context context){
		mContext = context;		
	}		
	
	public static String scan(	int width, 
								int height, 
								byte[] frameData, 
								boolean useLocation, 
								Boolean isResultConfident,
								Activity activity) throws WLAuthenticationException{
		
		mActivity = activity;			
		
		String result = WLImageScanner.scan(width, height, frameData,useLocation);		
		
		if(	result != null  ){
			if(result.startsWith(":")){
				result = result.substring(1, result.length());
			}
			if(result.endsWith(":")){
				result = result.substring(0, result.length()-1);
			}
		}
		else{
			result = NO_RESULT; 
		}
		Log.i("Main Search" , "Result!" + result);
		
		String[] resultList = result.split(":");
		
		boolean isValidated = false;		
		
		StringBuilder targetObject = new StringBuilder();
		
		if( useLocation &&
			validateImageScanFromDirectionComparison(targetObject)){
			Log.i("Main Search" , "Validated Cool !");
			isValidated=true;
		}
		
		isResultConfident = false;
		if(	resultList.length == 1 && 
			!resultList[0].isEmpty()&&
			!resultList[0].equalsIgnoreCase(NO_RESULT)){
			// We have a confident match..
			if(	!useLocation || 
				isValidated){
				isResultConfident = true;
			}
		}
		else if(resultList.length > 1){
			// This means we did not have confidence on our result.We got multiple result.
			// Check from the direction and give results.
			if(isValidated){
				for(int possibleResult = 0 ; possibleResult < resultList.length ; possibleResult++){
					Log.i("MainSearch",resultList[possibleResult]  + " : " + targetObject.toString());
					if(resultList[possibleResult].trim().equalsIgnoreCase(targetObject.toString().trim())){
						result = targetObject.toString();
						isResultConfident = true;							
						break;
					}
				}
			}
			else{
				isResultConfident = false;
			}
		}
		else{			
			result = NO_RESULT;
			if(	isValidated && 
				(RANGE.CLOSE.inRange(mCurrentDistance) && mIsCurrentDirectionExactSame)){
				//result = targetObject.toString();
				Log.i("Main Search" , "Validated Result!" + result);		
			}
			isResultConfident = false;
		}
		Log.i("Main Search" , "Final Result!" + result);
		return result;
	}
	
	
	private static boolean calculateDistanceandDirection(final StringBuilder targetObject){
		WLCompassManager cm = WLCompassManager.getInstance();
		mTargetLocation = getTargetLocation(targetObject);
		
		if(mTargetLocation == null){
			return false;
		}
		Log.i("MainSearch",mTargetLocation.getLatitude() + " " + mTargetLocation.getLongitude() + ":" + targetObject);
		
		if( !WLGeoLocationConstants.getInstance().isValidLocation()){
			return false;
		}
		
		final double distance = WLGeoLocationUtility.calculateDistance(
								WLGeoLocationConstants.getInstance().getLatitude(), 
								WLGeoLocationConstants.getInstance().getLongitude(), 
								mTargetLocation.getLatitude(), 
								mTargetLocation.getLongitude());		
		
		final DIRECTION targetDirection = WLGeoLocationUtility.getDirectionsBetweenGeoLocations(
								WLGeoLocationConstants.getInstance().getLatitude(), 
								WLGeoLocationConstants.getInstance().getLongitude(), 
								mTargetLocation.getLatitude(), 
								mTargetLocation.getLongitude());
								
		final DIRECTION cameraPointingDirection = cm.getDirection();
		Log.i("Direction123", "Camera is pointing toward " + cameraPointingDirection.name());
		
		
		mActivity.runOnUiThread( new Runnable(){

			@Override
			public void run() {
				WLCameraActivity.mDirRes.setText("GeoLocation Direction:" + targetDirection.name() +"-Camera Direction:"+cameraPointingDirection.name()+"-Distance:"+distance);				
			}
		});
		
		mIsCurrentDirectionApproxSame = DIRECTION.doApproximateComparison(cameraPointingDirection, targetDirection);
		mIsCurrentDirectionExactSame = DIRECTION.doExactComparison(cameraPointingDirection, targetDirection);
		mCurrentDistance = distance;
		
		
		
		return true;
	}
	
	private static boolean validateImageScanFromDirectionComparison(StringBuilder targetObject){
		boolean ret = false;
		if( calculateDistanceandDirection(targetObject) ){
			if( /*(RANGE.TOO_FAR.inRange(distance) || 
			 	RANGE.TOO_NEAR.inRange(distance) ) ||*/
					(RANGE.FAR.inRange(mCurrentDistance) && mIsCurrentDirectionApproxSame) ||
					(RANGE.CLOSE.inRange(mCurrentDistance) && mIsCurrentDirectionApproxSame )){			
					WLGeoLocationUtility.setIfDirectionValidated(true);
					ret = true;					
			}else{
				WLGeoLocationUtility.setIfDirectionValidated(false);
			}
		}
		Log.i("Direction", "Is Direction Same" + Boolean.valueOf(ret).toString());
		return ret;
	}

	private static Location getTargetLocation(StringBuilder targetObject) {
		WLLocationObjectsManager lo = new WLLocationObjectsManager(WLGeoLocationConstants.getInstance().getLocation());		 
		return lo.pickBestTarget(targetObject);
	}
}

