package wazzatimagescanner;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.ProgressBar;


/* Interact with the online servers */
public abstract class WLServerManager extends AsyncTask<String, Integer, String>{

	/* Authorized Token */
	private static String AUTHENTICATION_TOKEN = "";

	private static final String AUTHENTICATION_HTTP_URL = "http://www.wazzatlabs.com/sdkserver.php?";
	
	//private static WLFileDownloader fileDownloader = new WLFileDownloader();
	
	private Handler mHandler = new Handler();

	private static WLAssetHelper mAssetHelper =  new WLAssetHelper();	
	
	public boolean isConnected(){
		ConnectivityManager cm =
				(ConnectivityManager)WLClientAcitivityInfo.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null &&
				activeNetwork.isConnectedOrConnecting();
		return isConnected;
	}

	private String convertInputStreamToString(InputStream inputStream) throws IOException{
		BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
		String line = "";
		String result = "";
		while((line = bufferedReader.readLine()) != null)
			result += line;

		inputStream.close();
		return result;
	}

	public String getJsonFromServer(String url){
		String result = "";

		URL uri = null;
		HttpURLConnection urlConnection = null;
		try {
			uri = new URL(url);


			urlConnection = (HttpURLConnection) uri.openConnection();
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			// convert inputstream to string
			result = null;			

			if(in != null)
				result = convertInputStreamToString(in);


		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		}finally{

			urlConnection.disconnect();
		}

		return result;
	}
	// Progress Status is revognized our of 100
	private void updateProgressBar(final int progresStatus){
		final ProgressBar progressBar = getProgressBar();
		if(mHandler !=null && progressBar != null){
			mHandler.post(new Runnable() {
				public void run() {
					progressBar.setProgress(progresStatus);					
				}
			});
		}
	}

	@Override
	protected String doInBackground(String... params) {
		AUTHENTICATION_TOKEN = params[0] ;
		String json_result = null;
		if( isConnected() ){
			//String postUrl = AUTHENTICATION_HTTP_URL +AUTHENTICATION_TOKEN ;
			
			String deviceId = Secure.getString(WLClientAcitivityInfo.getContext().getContentResolver(),
	                Secure.ANDROID_ID);
			try {
				deviceId = URLEncoder.encode(deviceId, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				Log.i("Authentication Failer"," Can not authentica te url:" + deviceId);
				e.printStackTrace();
			}
			String postUrl = AUTHENTICATION_HTTP_URL+"token="+AUTHENTICATION_TOKEN+"&device="+deviceId;
									
			Log.i("URL ENCODED", postUrl);
			updateProgressBar(10);
			json_result = getJsonFromServer(postUrl );
			updateProgressBar(20);
			Log.i("WLauthenticate","Response: "+json_result);
			if( json_result == null || 
				//!fileDownloader.startDownloading( getProgressBar()) 
				!mAssetHelper.startCopy(getProgressBar()) 	){
				Log.e("DataDownloader","Failed to download the data");
				return null;
			}
			updateProgressBar(80);
			Log.i("DataLoader","Loading data....");			
			if( !WLData.loadData() ){
				Log.e("DataDownloader","Data load failed");
				return null;
			}
			Log.i("DataLoader","Data load completed");
			updateProgressBar(90);
		}		
		return json_result;	
	}

	@Override
	protected abstract void onProgressUpdate(Integer... values) ;	

	@Override
	protected abstract void onPostExecute(String result);
	
	protected abstract ProgressBar getProgressBar();

}
