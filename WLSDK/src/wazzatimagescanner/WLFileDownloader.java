package wazzatimagescanner;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;

public class WLFileDownloader{

	static final String WL_URL = "http://www.wazzatlabs.com/downloads/";

	static String WL_THUMB_URL = "http://ec2-54-187-161-91.us-west-2.compute.amazonaws.com/AppData/uploads/";

	static String WL_SERVER_PHP_URL="";

	static final ArrayList<String> FILE_LISTS = new ArrayList<String>(){

		private static final long serialVersionUID = 1L;

		{		
			add("Annotations");
			add("DCount");
			add("InvertedIndex");
			add("NumImages");
			add("Objects");
			add("Locations");
			add("ObjectsChild");

		}
	};

	Handler mHandler = new Handler();
	ProgressBar mProgressBar = null;
	int mProgressStatus= 30;

	public WLFileDownloader()
	{
		WL_SERVER_PHP_URL = "http://www.wazzatlabs.com/syncserver_2.php?token="+WLClientAcitivityInfo.getToken()+"&mode=DOWN";

		WL_THUMB_URL += "user_" + WLClientAcitivityInfo.getToken() + "/";
	}

	private boolean  executeServerScript() {
		boolean ret = false;

		URL uri = null;
		HttpURLConnection urlConnection = null;
		try {
			uri = new URL(WL_SERVER_PHP_URL);
			Log.i("Server Sync","server script URL:" + WL_SERVER_PHP_URL);


			urlConnection = (HttpURLConnection) uri.openConnection();
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			String result = "";			

			if(in != null){
				BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(in));
				String line = "";
				while((line = bufferedReader.readLine()) != null)
					result += line;

				in.close();				
			}
			Log.i("Download Server Sync","Result:" + result);
			if(result.trim().equalsIgnoreCase("OK")){
				ret = true;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();	
		}finally{
			urlConnection.disconnect();
		}
		return ret;		
	}


	// Progress Status is revognized our of 100
	private void updateProgressBar(final int progresStatus){

		if(mHandler !=null  && mProgressBar !=null){
			mHandler.post(new Runnable() {
				public void run() {
					mProgressBar.setProgress(progresStatus);					
				}
			});
		}
	}


	protected boolean startDownloading(ProgressBar progressBar) {
		mProgressBar = progressBar;
		mProgressStatus = 25;
		/* First download all the data files */
		boolean ret = executeServerScript();
		for(int i=0 ; i < FILE_LISTS.size() ; ++i){
			String file_name = FILE_LISTS.get(i);
			if(	WLClientAcitivityInfo.useGV() && 
				file_name.equalsIgnoreCase("InvertedIndex")){				
				ret = ret && downloadInvertedIndexGV();					
			}else{
				ret = ret && download(file_name);
			}						
			updateProgressBar(mProgressStatus);
			mProgressStatus+=5;
		}	
		/* Now we will download the thumbnails */
		ret = ret && downloadThumbnails();
		ret =  downloadHKMeans() && ret;

		return ret;

	}

	private boolean download(String file_name) {
		int count;
		boolean ret = true;
		try {
			String url_str = WL_URL + file_name + "_" + WLClientAcitivityInfo.getToken() + ".txt";
			Log.i("WLFileDownloaer","Downloading File from this url : " +  url_str);
			URL url = new URL(url_str);
			URLConnection conection = url.openConnection();
			conection.connect();

			// input stream to read file - with 8k buffer
			InputStream input = new BufferedInputStream(url.openStream(), 8192);

			String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
			String applicationName =  WLClientAcitivityInfo.getApplicationName();
			/* Preparing the directory  */
			File directory = new File(externalStorage+"/"+applicationName);
			directory.mkdirs();
			String output_url = externalStorage+"/"+applicationName + "/"+ file_name +".txt";
			// Output stream to write file
			OutputStream output = new FileOutputStream(output_url);

			byte data[] = new byte[1024];

			while ((count = input.read(data)) != -1) {
				output.write(data, 0, count);
			}

			// flushing output
			output.flush();

			// closing streams
			output.close();
			input.close();
			Log.i("WLFileDownloaer","Downloading complete from this url : " +  url_str);
		} catch (Exception e) {
			ret = false;
			Log.e("Error: ", e.getMessage());
		}
		return ret;
	}

	private boolean downloadThumbnails() {

		boolean ret = true;

		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		String applicationName =  WLClientAcitivityInfo.getApplicationName();
		String inputFileURL = externalStorage + "/" + applicationName +"/Objects.txt" ;

		BufferedReader br = null;
		int count;
		try {
			br = new BufferedReader(new FileReader(inputFileURL));
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				String[] split =sCurrentLine.split(" ");
				if(split.length == 3){
					String file_name = "thumbs/"+split[2];
										
					String url_str = WL_THUMB_URL + file_name + "_thumb.jpg";
					Log.i("WLFileDownloaer","Downloading File from this url : " +  url_str);
					URL url = new URL(url_str);
					URLConnection conection = url.openConnection();
					conection.connect();

					// input stream to read file - with 8k buffer
					InputStream input =null;
					try{
						input = new BufferedInputStream(url.openStream(), 8192);
					}catch( IOException e){
						continue;
					}
					
					/* Preparing the directory  */
					File directory = new File(externalStorage+"/"+applicationName+"/thumbs");
					directory.mkdirs();					
					String output_url = externalStorage+"/"+applicationName + "/"+ file_name +"_thumb.jpg";
					// Output stream to write file
					OutputStream output = new FileOutputStream(output_url);

					byte data[] = new byte[1024];

					while ((count = input.read(data)) != -1) {
						output.write(data, 0, count);
					}

					// flushing output
					output.flush();

					// closing streams
					output.close();
					input.close();
					Log.i("WLFileDownloaer","Downloading complete from this url : " +  url_str);
				}
				updateProgressBar(mProgressStatus);
				mProgressStatus+=5;
			}
			br.close();
		} catch (IOException e) {
			ret = true; // Does not care much about thumbnails
			e.printStackTrace();
		}
		return ret;
	}
	
	
	private boolean downloadHKMeans() {
		int count;
		boolean ret = true;
		try {
			String url_str = "http://ec2-54-187-161-91.us-west-2.compute.amazonaws.com/AppData/uploads/user_" + WLClientAcitivityInfo.getToken() + "/HKMeans_10000.Tree";
			Log.i("WLFileDownloaer","Downloading File from this url : " +  url_str);
			URL url = new URL(url_str);
			URLConnection conection = url.openConnection();
			conection.connect();

			// input stream to read file - with 8k buffer
			InputStream input = new BufferedInputStream(url.openStream(), 8192);

			String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
			String applicationName =  WLClientAcitivityInfo.getApplicationName();
			/* Preparing the directory  */
			File directory = new File(externalStorage+"/"+applicationName);
			directory.mkdirs();
			String output_url = externalStorage+"/"+applicationName + "/"+ "HKMeans_10000.Tree";
			// Output stream to write file
			OutputStream output = new FileOutputStream(output_url);

			byte data[] = new byte[1024];

			while ((count = input.read(data)) != -1) {
				output.write(data, 0, count);
			}

			// flushing output
			output.flush();

			// closing streams
			output.close();
			input.close();
			Log.i("WLFileDownloaer","Downloading complete from this url : " +  url_str);
		} catch (Exception e) {
			ret = false;
			Log.e("Error: ", e.getMessage());
		}
		return ret;
	}
	private boolean downloadInvertedIndexGV() {
		int count;
		boolean ret = true;
		try {
			String url_str = "http://ec2-54-187-161-91.us-west-2.compute.amazonaws.com/AppData/uploads/user_" + WLClientAcitivityInfo.getToken() + "/InvertedIndex_GV.txt";
			Log.i("WLFileDownloaer","Downloading File from this url : " +  url_str);
			URL url = new URL(url_str);
			URLConnection conection = url.openConnection();
			conection.connect();

			// input stream to read file - with 8k buffer
			InputStream input = new BufferedInputStream(url.openStream(), 8192);

			String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
			String applicationName =  WLClientAcitivityInfo.getApplicationName();
			/* Preparing the directory  */
			File directory = new File(externalStorage+"/"+applicationName);
			directory.mkdirs();
			String output_url = externalStorage+"/"+applicationName + "/"+ "InvertedIndex_GV.txt";
			// Output stream to write file
			OutputStream output = new FileOutputStream(output_url);

			byte data[] = new byte[1024];

			while ((count = input.read(data)) != -1) {
				output.write(data, 0, count);
			}

			// flushing output
			output.flush();

			// closing streams
			output.close();
			input.close();
			Log.i("WLFileDownloaer","Downloading complete from this url : " +  url_str);
		} catch (Exception e) {
			ret = false;
			Log.e("Error: ", e.getMessage());
		}
		return ret;
	}

	
}
