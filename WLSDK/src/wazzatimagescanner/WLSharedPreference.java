package wazzatimagescanner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class WLSharedPreference {

	private String APPLICATION_KEY ;
		
	private String TOKEN_EXPIRATION_KEY ;
	
	private String TOKEN_ID_KEY ;
	
	private String API_CALLS_COUNT_KEY ;
	
	private String DATA_VERSION_KEY ;
		
	private final long  HUNDRED_DAYS_IN_SECOND = 8640000;
	
	private boolean isExpired = true;
	
	private boolean isFirstStart = false;
	
	SharedPreferences mSharedPref;
	
	WLNative mNativeInvoker = new WLNative();
	
	WLTimeManger mTimeManager = new WLTimeManger();
	
	private static WLSharedPreference mInstance = null;
	
	public static WLSharedPreference getInstance(){
		if( mInstance == null){
			mInstance = new WLSharedPreference();
		}
		return mInstance;
	}	
	
	private WLSharedPreference(){
		APPLICATION_KEY = mNativeInvoker.getSharedPrefAppKey();
		TOKEN_EXPIRATION_KEY = mNativeInvoker.getSharedPrefTokenExpKey();
		DATA_VERSION_KEY = mNativeInvoker.getSharedPrefVersionKey();
		API_CALLS_COUNT_KEY = mNativeInvoker.getSharedPrefScanCountKey();
		TOKEN_ID_KEY = mNativeInvoker.getSharedPrefTokenIDKey();
		
		loadPref();

	}

	private void loadPref() {
		mSharedPref = WLClientAcitivityInfo.getContext().getSharedPreferences(APPLICATION_KEY, Context.MODE_PRIVATE);				
		
		if(mSharedPref.contains(TOKEN_EXPIRATION_KEY)){
			Log.i("WLShared Pref", " TOKEN is already created" );

			String registeredTime = mSharedPref.getString(TOKEN_EXPIRATION_KEY, "");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",Locale.US);  
			Date registeredDate = null;
			try {
				registeredDate = df.parse(registeredTime);
			} catch (ParseException e) {
				e.printStackTrace();
			}  
			Log.i("WLShared Pref", " Registered data " + registeredDate + " :" + registeredTime );
			isExpired = false;
			if(registeredDate != null){
				long diffInSeconds = Math.abs((mTimeManager.getCurrentSystemTimeInMilli()/1000)- (registeredDate.getTime()/1000));			
				if( diffInSeconds > HUNDRED_DAYS_IN_SECOND ){
					Log.i("WLShared Pref", " Expired product " + diffInSeconds );
					isExpired = true;
				}
			}
		}
		else{
			Log.i("WLShared Pref", " Its new" );
			Editor prefEditor =  mSharedPref.edit();
			prefEditor.putString(TOKEN_EXPIRATION_KEY, "2014-01-01 00:00:00");
			prefEditor.commit();
			isFirstStart = true;
			isExpired = false;
		}		
	}
	
	public boolean isCurrentSDKExpired(){
		return isExpired;
	}
	
	public boolean isFirstStart(){
		return isFirstStart;
	}
	
	public void setRegistrationDate(String date){
		Editor prefEditor =  mSharedPref.edit();
		prefEditor.putString(TOKEN_EXPIRATION_KEY, date);
		prefEditor.commit();
		isExpired = false;
	}
	
	public void setVersionNumber(int version){
		Editor prefEditor =  mSharedPref.edit();
		prefEditor.putInt(DATA_VERSION_KEY, version);
		prefEditor.commit();
	}
	public int getVersionNumber(){
		return mSharedPref.getInt(DATA_VERSION_KEY, 1);
	}
	
	public void incrementAPICallsCount(){
		int count = 1;
		if(mSharedPref.contains(API_CALLS_COUNT_KEY)){
			count = mSharedPref.getInt(API_CALLS_COUNT_KEY, 1);
			count++;
		}
		Editor prefEditor =  mSharedPref.edit();
		prefEditor.putInt(API_CALLS_COUNT_KEY, count);
		prefEditor.commit();		
	}
	
	public int getAPICallsCount(){
		return mSharedPref.getInt(API_CALLS_COUNT_KEY, 0);	
	}
	
	public void setTokenID(String token){
		Editor prefEditor =  mSharedPref.edit();
		prefEditor.putString(TOKEN_ID_KEY, token);
		prefEditor.commit();
		isExpired = false;
	}
	
	public String getTokenID(){
		return mSharedPref.getString(TOKEN_ID_KEY, "");
	}
	
}
