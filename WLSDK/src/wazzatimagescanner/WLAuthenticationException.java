package wazzatimagescanner;


/**
 * 
 * Wazzzat Custom Exception. This is thrown when the current
 * application is not authenticated. 
 * 
 * @author wazzat Labs
 *
 */
public class WLAuthenticationException extends Exception {

	private static final long serialVersionUID = 1L;

	
	private String message = "Authentication failed with the provided token id. Please contact wazzatlabs for the correct Token ID";
	 
    public WLAuthenticationException() {
        super();
    }
 
    public WLAuthenticationException(String message) {
        super(message);
        this.message = message;
    }
 
    public WLAuthenticationException(Throwable cause) {
        super(cause);
    }
 
    @Override
    public String toString() {
        return message;
    }
 
    @Override
    public String getMessage() {
        return message;
    }
}
