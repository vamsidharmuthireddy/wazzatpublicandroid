package wazzatimagescanner;

public class Direction {

	public static enum DIRECTION{
		NORTH(1),
		NORTH_EAST(2),
		EAST(3),
		SOUTH_EAST(4),			
		SOUTH(5),
		SOUTH_WEST(6),		
		WEST(7),
		NORTH_WEST(8),			
		NOT_ABLE_DETECT(9) ;
		
		private int mValue;
		
		private DIRECTION(int value){
			mValue = value;
		}
		
		public int getValue(){
			return mValue;
		}
		public static boolean doExactComparison(DIRECTION source,DIRECTION target){			
			return source == target;
		}

		public static boolean doApproximateComparison(DIRECTION source,DIRECTION target){
			boolean ret = false;

			if( (target == NORTH &&
				(	source == target || 
					source.getValue() == target.getValue()+1 || 
					source == NORTH_WEST
				)) ||
				( target == NORTH_WEST &&
				  (	source == target || 
				  	source.getValue() == target.getValue()-1 || 
				  	source == NORTH
				 ))	||
				 (	source == target || 
					source.getValue() == target.getValue()-1 || 
					source.getValue() == target.getValue()+1
			)){
				ret = true;
			}
			return ret;
		}
	}
}
