package wazzatimagescanner;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class WLData {

	static boolean isFirstCall = true;  /* Populate the data on first call */

	static boolean isDataModified = false;  /* Tracks the update of model List */

	static ArrayList<String> mDefinedObjects = null;

	static WLNative	 mNativeInvoker = new WLNative();

	static byte[] lastCapturedImageData = null;

	public static boolean loadData() {

		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		String applicationName=WLClientAcitivityInfo.getApplicationName();
		Log.i("WLData","Storing the relevant file in:" + externalStorage+"/"+applicationName);	
		
		mNativeInvoker.UpdatePackageInfo(externalStorage, applicationName);
		//mNativeInvoker.UpdatePackageInfo("/sdcard", "Carz");
		isDataModified = true;
		return mNativeInvoker.LoadData(WLClientAcitivityInfo.useGV()); 
	}

	public static void updateData(String newName) throws WLAuthenticationException{
		if( !WLAuthenticate.isUserAuthenticated() ){
			throw new WLAuthenticationException();
		}

		/* Replacing all the special character with '_' */
		String modifiedName = newName.trim().replaceAll("\\W", "_").toUpperCase(Locale.US);
		
		Log.i("WLDATA","Modified name " + modifiedName+":" + newName);


		
		mNativeInvoker.UpdateData(modifiedName);
		if(!modifiedName.isEmpty()){
			mNativeInvoker.UpdateDataLocal();
			isDataModified = true;
			getObjectList();

			///* Need to add the corresponding image in the thumbs folder */
			String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
			String applicationName=WLClientAcitivityInfo.getApplicationName();		
			String thumbPath = externalStorage + "/" + applicationName + "/" + "thumbs";

			Log.i("WLModifier","Thumnail upload using folder " + thumbPath);

			File thumbDir = new File(thumbPath);
			thumbDir.mkdir();
			// Image is added in the native

			Log.i("WLModifier","Database updated with " + modifiedName);

		}				
	}

	public static ArrayList<String> getObjectList() throws WLAuthenticationException{
		if( !WLAuthenticate.isUserAuthenticated() ){
			throw new WLAuthenticationException();
		}
		if( isDataModified || isFirstCall ){
			String[] objectList = mNativeInvoker.GetObjectsList();
			mDefinedObjects =  new ArrayList<String>(Arrays.asList(objectList));
			isFirstCall = false;
		}
		return mDefinedObjects;
	}
	
	public static ArrayList<String> getChildObjects(String parentName) throws WLAuthenticationException{
		if( !WLAuthenticate.isUserAuthenticated() ){
			throw new WLAuthenticationException();
		}
		String[] objectList = mNativeInvoker.GetObjectsChildList(parentName);
		return new ArrayList<String>(Arrays.asList(objectList));
	}

	protected static boolean isLocalDataModified(){
		return isDataModified;
	}
	
	/*private static void storeLastImageAsThumbnail(String name){


		Log.i("WLModifier","Storing thumbnail for  " + name);

		Bitmap bitmap = null;
		if(WLImageManager.getInstance().isImagePathUsedLast()){
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			bitmap = BitmapFactory.decodeFile(WLImageManager.getInstance().getLastProcessedImagePath(), options);
		}
		else{
			byte[] bitmapdata = WLImageManager.getInstance().getLastProcessedImageData();
			if(bitmapdata == null){
				Log.i("WLModifier","bitmapdata is null here");
			}

			int nrOfPixels = bitmapdata.length / 3; // Three bytes per pixel.
			int pixels[] = new int[nrOfPixels];
			for(int i = 0; i < nrOfPixels; i++) {
				int r = bitmapdata[3*i];
				int g = bitmapdata[3*i + 1];
				int b = bitmapdata[3*i + 2];
				pixels[i] = Color.rgb(r,g,b);
			}
			//bitmap = Bitmap.createBitmap(pixels, , , Bitmap.Config.ARGB_8888);
			bitmap = BitmapFactory.decodeByteArray(bitmapdata , 0, bitmapdata .length);
			if(bitmap == null){
				Log.i("WLModifier","bitmap is null here");
			}
		}

		File filethumb;
		Bitmap thumb = null;
		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		String applicationName=WLClientAcitivityInfo.getApplicationName();		
		String thumbPath = externalStorage + "/" + applicationName + "/" + "thumbs";

		Log.i("WLModifier","Thumnail upload using folder " + thumbPath);
 
		File thumbDir = new File(thumbPath);
		thumbDir.mkdir();
		try {

			filethumb = new File(thumbPath, name);

			FileOutputStream out = new FileOutputStream(filethumb);

			thumb = Bitmap.createScaledBitmap(bitmap, 75, 75, false);
			thumb.compress(Bitmap.CompressFormat.JPEG, 90, out);

			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	@SuppressLint("NewApi")
	public static Bitmap getThumbnail(String name){
		String modifiedName = name.trim().replace("\\W", "_");
		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		String applicationName=WLClientAcitivityInfo.getApplicationName();		
		String thumbPath = externalStorage + "/" + applicationName + "/thumbs/" +modifiedName+"_thumb.jpg";
		Log.i("WLModifier","Fetching thumbnail from: " + thumbPath);

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		File file = new File(thumbPath);
		Bitmap thumb = null;
		if(file.exists()){
			Bitmap bitmap= BitmapFactory.decodeFile(thumbPath, options);
			if(bitmap != null){
				WindowManager wm =(WindowManager) WLClientAcitivityInfo.getContext().getSystemService(Context.WINDOW_SERVICE);
				Display dp = wm.getDefaultDisplay();
				Point size = new Point();
				int width = 0;
				int height = 0;
				if( Integer.valueOf(android.os.Build.VERSION.SDK_INT) >= 16){
					dp.getSize(size);
					width=size.x;
					height=size.y;
				}
				else{
					width = dp.getWidth();
					height = dp.getHeight();
				}
				
				int bmpheight = height/6;
				int bmpWidth = bmpheight;
						
				thumb = Bitmap.createScaledBitmap(bitmap, bmpWidth, bmpheight, false);
			}
		}
		
		Log.i("WLModifier","scaledBitmap" + thumb);

		return thumb;     		
	}
	
	
	public static void setLastCapturedImage(byte[] data){
		lastCapturedImageData = data;
	}
	public static byte[] getLastCaptureImage(){
		return lastCapturedImageData ;
	}
	
	public static void setGeoLocationUseOn(){
		mNativeInvoker.setGeoLocationUseOn();
	}
	public static void setGeoLocationUseOff(){
		mNativeInvoker.setGeoLocationUseOff();
	}
	
	public static void toggleUseGV(){
		mNativeInvoker.toggleUseGV();
	}
	
}
