package wazzatimagescanner;

import java.util.ArrayList;

public class WLConstants {

	static final ArrayList<String> FILE_LISTS = new ArrayList<String>(){

		private static final long serialVersionUID = 1L;

		{		
			add("Annotations.txt");
			add("DCount.txt");
			add("InvertedIndex.txt");
			add("NumImages.txt");
			add("Objects.txt");

		}
	};
}
