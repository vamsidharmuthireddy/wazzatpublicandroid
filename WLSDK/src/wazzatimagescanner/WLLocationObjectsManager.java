package wazzatimagescanner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import android.location.Location;
import android.os.Environment;
import android.util.Log;

/** 
 * Manages the map of different objects to their geolocation.	 
 * 
 * 
 * @author wazzat
 *
 */
public class WLLocationObjectsManager {
	
	
	private class TargetDistance implements Comparable<TargetDistance>{
		private Location mLocation;
		private Double mDistance;
		
		public TargetDistance(Location location, double d) {
			mLocation=location;
			mDistance=Double.valueOf(d);
		}
		
		public Location getLocation(){
			return mLocation;
		}
		public Double getDistance(){
			return mDistance;
					
		}
		public void setDistance(double d){
			mDistance = Double.valueOf(d);					
		}

		@Override
		public int compareTo(TargetDistance another) {
			int ret = 0;
			
			if(mDistance < another.getDistance()) ret = -1;
			else if(mDistance > another.getDistance()) ret = 1;
			
			return ret;
		}
	}

	private static final String LOCATION_FILE_NAME = "Locations.txt";
	
	private HashMap<Location, String> mLocationObject = new HashMap<Location,String>();
	
	private ArrayList<TargetDistance>  mSortedLocation = new ArrayList<TargetDistance>();
	
	private Location mBaseLocation ;

	public WLLocationObjectsManager(Location initialBaseLocation){
		mBaseLocation = initialBaseLocation;
		fillMap();
		sortLocationByBaseDistance();
	}

	private void fillMap(){
		BufferedReader br = null;
		try {
			String sCurrentLine;
			String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
			String applicationName =  WLClientAcitivityInfo.getApplicationName();		
			String path = externalStorage+File.separator+	applicationName + File.separator + LOCATION_FILE_NAME;
			
			br = new BufferedReader(new FileReader(path));

			while ((sCurrentLine = br.readLine()) != null) {
				String[] temp = sCurrentLine.split(" "); //split by space
				if( temp.length == 3){
					Location tempLoc = new Location(temp[0]);
					String tempObject = temp[0];
					tempLoc.setLatitude(Double.parseDouble(temp[1]));
					tempLoc.setLongitude(Double.parseDouble(temp[2]));
					mLocationObject.put(tempLoc, tempObject);
					Log.i("MainSearch",tempLoc.getLatitude() + " " + tempLoc.getLongitude() + "-" + tempObject);
					mSortedLocation.add(new TargetDistance( 
												tempLoc, 
												WLGeoLocationUtility.calculateDistance(	
													mBaseLocation.getLatitude(), 
													mBaseLocation.getLongitude(), 
													tempLoc.getLatitude(), 
													tempLoc.getLongitude()
												)));
				}				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public void recaliberate(Location baseLocation){
		mBaseLocation = baseLocation;
		
		for(int i = 0 ; i < mSortedLocation.size() ; ++i){
			TargetDistance target  = mSortedLocation.get(i);
			target.setDistance(
					WLGeoLocationUtility.calculateDistance(	
						mBaseLocation.getLatitude(), 
						mBaseLocation.getLongitude(), 
						target.getLocation().getLatitude(), 
						target.getLocation().getLongitude()));
		}
		sortLocationByBaseDistance();
	}
	
	public Location pickBestTarget(StringBuilder targetObject){
		Location bestPick = null;
		if( mSortedLocation.size() > 0 ){
			bestPick = mSortedLocation.get(0).getLocation();
			
			targetObject.append(mLocationObject.get(bestPick));
			
			Log.i("MainSearch",bestPick.getLatitude() + " " + bestPick.getLongitude() + ":" + targetObject.toString());
		}		
		return bestPick;	
	}
	
	// Location is sorted as per the distance from the initial base location, which is generally the current geo location.
	private void sortLocationByBaseDistance(){
		Collections.sort(mSortedLocation);
	}
}
