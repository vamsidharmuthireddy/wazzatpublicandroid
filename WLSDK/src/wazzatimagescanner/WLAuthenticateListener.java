package wazzatimagescanner;

import android.widget.ProgressBar;


/**
 * 
 * Implement this interface to perform the custom functionality once the
 * authenticate thread is completed.
 * 
 * Authenticate class uses this interface to post the thread state.
 * 
 * @author wazzat Labs
 *
 */
public interface WLAuthenticateListener {
	
	abstract void whileAuthenticating();
	
	abstract void onAuthenticationSuccess();
	
	abstract void onAuthenticationFailure(int error_code);
	
	abstract ProgressBar getProgressBar();

}
