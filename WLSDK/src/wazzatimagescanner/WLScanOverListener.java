package wazzatimagescanner;

/**
 * This is an observer class which is invoked 
 * when card is completed
 *
 */
public interface WLScanOverListener {
	void onScanDone(boolean success, String result) ;
}
