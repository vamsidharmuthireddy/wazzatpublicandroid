package wazzatimagescanner;

/**
 * 
 * @author wazzat
 * 
 *         Contains the function call to native c methods.
 * 
 */
class WLNative {

	/**
	 * Native call to process the image and get the recognized text.
	 * 
	 * @param width
	 *            - Camera Preview width of the image.
	 * @param height
	 *            - Camera Preview height of the image
	 * @param frameData
	 *            - Data matrix of the provided image.
	 * @param bgra
	 *            - Processed RGBA frame
	 * @param success
	 *            - Returns the status code to check if the processing is
	 *            successful or not.
	 * 
	 * @return Recognized text from the image.
	 */
	protected native String Search(int width, int height, byte frameData[],
			int[] bgra, int[] success, double latitude, double longitude);

	protected native String SearchPath(String path, int[] success);

	protected native void FindFeatures(int width, int height, byte frameData[],
			int[] bgra);

	protected native boolean LoadData(boolean useGV);

	protected native void UpdatePackageInfo(String externalStorage,
			String packageName);

	protected native void UpdateData(String name);

	protected native void UpdateDataLocal();

	protected native String[] GetObjectsList();

	protected native String[] GetObjectsChildList(String anme);

	protected native String getSharedPrefAppKey();

	protected native String getSharedPrefTokenExpKey();

	protected native String getSharedPrefVersionKey();

	protected native String getSharedPrefScanCountKey();

	protected native String getSharedPrefTokenIDKey();

	protected native void setGeoLocationUseOn();

	protected native void setGeoLocationUseOff();
	
	protected native void toggleUseGV();

	protected double sendGeoDistanceToNative(double lat1, double lon1,
			double lat2, double lon2) {

		return WLGeoLocationUtility.calculateDistance(lat1, lon1, lat2, lon2);
	}

	protected boolean isDirectionValidated() {

		return WLGeoLocationUtility.isDirectionValidated();
	}

	static {
		System.loadLibrary("opencv_java");
		System.loadLibrary("wlimagescanner");
	}

	/**
	 * 
	 * Empty protected constructor. Instance can be made only within the same
	 * package.
	 */
	protected WLNative() {

	}

}
