package wazzatimagescanner.facebook;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import wazzatimagescanner.WLData;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;

public class FacebookActivity extends FragmentActivity {

    private LoginButton loginBtn;
    private Button postImageBtn;
    private Button updateStatusBtn;
 
    private TextView userName;
 
    private UiLifecycleHelper uiHelper;
 
    private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
    private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
    private boolean pendingPublishReauthorization = false;
 
    private static String message = "Checking facebook Test SDK";
    
    public static int id_activity =0;
    public static int id_user_name =0;
    public static int id_login =0;
    public static int id_post =0;
    public static int id_status =0;
   
    private ProgressDialog mProgressDoalog = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        uiHelper = new UiLifecycleHelper(this, statusCallback);
        uiHelper.onCreate(savedInstanceState);
 
        setContentView(id_activity);
 
        userName = (TextView) findViewById(id_user_name);
        
        message = getIntent().getStringExtra("MESSAGE");
        
        loginBtn = (LoginButton) findViewById(id_login);
        loginBtn.setUserInfoChangedCallback(new UserInfoChangedCallback() {
            @Override
            public void onUserInfoFetched(GraphUser user) {
                if (user != null) {
                    userName.setText("Hello, " + user.getName());
                    mProgressDoalog = new ProgressDialog(FacebookActivity.this);
                    mProgressDoalog.setIndeterminate(true);
                    mProgressDoalog.setMessage("Working...");
                    mProgressDoalog.setTitle("Facebook update");
                    mProgressDoalog.show();
                    postImage();     
                    publishStory();
                } else {
                	userName.setVisibility(View.INVISIBLE);
                    userName.setText("You are not logged");
                }
            }
        });
 
        postImageBtn = (Button) findViewById(id_post);
        postImageBtn.setOnClickListener(new OnClickListener() {
 
            @Override
            public void onClick(View view) {
                postImage();
            }
        });
        postImageBtn.setVisibility(View.INVISIBLE);
 
        updateStatusBtn = (Button) findViewById(id_status);
        updateStatusBtn.setOnClickListener(new OnClickListener() {
 
            @Override
            public void onClick(View v) {
            	postStatusMessage();
            }
        });
        updateStatusBtn.setVisibility(View.INVISIBLE);
 
        buttonsEnabled(false);
    }

    
    
    
    private Session.StatusCallback statusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state,
                Exception exception) {
            if (state.isOpened()) {
                buttonsEnabled(true);
                Log.d("FacebookSampleActivity", "Facebook session opened");
            } else if (state.isClosed()) {
                buttonsEnabled(false);
                Log.d("FacebookSampleActivity", "Facebook session closed");
            }
        }
    };
 
    public void buttonsEnabled(boolean isEnabled) {
        postImageBtn.setEnabled(isEnabled);
        updateStatusBtn.setEnabled(isEnabled);
    }
 
    public void goBack(){
    	onBackPressed();
    }
    
    public void publishStory() {
        Session session = Session.getActiveSession();

        if (session != null){

            // Check for publish permissions    
            List<String> permissions = session.getPermissions();
            if (!isSubsetOf(PERMISSIONS, permissions)) {
                pendingPublishReauthorization = true;
                Session.NewPermissionsRequest newPermissionsRequest = new Session
                        .NewPermissionsRequest(this, PERMISSIONS);
            session.requestNewPublishPermissions(newPermissionsRequest);
                return;
            }

            Bundle postParams = new Bundle();
            postParams.putString("name", "WazzatTour");
            postParams.putString("caption", "Know what you see.");
            postParams.putString("description", "Wazzat tour helps the tourist to get involved with the site.It provides rish content on the press of a button!");
            postParams.putString("link", "http://www.wazzatlabs.com");
            postParams.putString("picture", "http://wazzatlabs.com/main/img/WazzatLabs.png");

            Request.Callback callback= new Request.Callback() {
                public void onCompleted(Response response) {
                    JSONObject graphResponse = response
                                               .getGraphObject()
                                               .getInnerJSONObject();
                    String postId = null;
                    try {
                        postId = graphResponse.getString("id");
                    } catch (JSONException e) {
                        Log.i("Facebook",
                            "JSON error "+ e.getMessage());
                    }
                    
                    FacebookRequestError error = response.getError();
                    if (error != null) {
                        Toast.makeText(FacebookActivity.this
                             .getApplicationContext(),
                             error.getErrorMessage(),
                             Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(FacebookActivity.this
                                 .getApplicationContext(), 
                                 postId,
                                 Toast.LENGTH_LONG).show();
                    }
                    if( mProgressDoalog!=null){
                    	mProgressDoalog.dismiss();
                    }
                    FacebookActivity.this.goBack();
                }
            };

            Request request = new Request(session, "me/feed", postParams, 
                                  HttpMethod.POST, callback);

            RequestAsyncTask task = new RequestAsyncTask(request);
            task.execute();
        }
    }
    
    private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }
        return true;
    }
    
    
    
    public void postImage() {
        if (checkPermissions()) {
            Bitmap img =BitmapFactory.decodeByteArray(WLData.getLastCaptureImage(),0,WLData.getLastCaptureImage().length);
            Request uploadRequest = Request.newUploadPhotoRequest(
                    Session.getActiveSession(), img, new Request.Callback() {
                        @Override
                        public void onCompleted(Response response) {
                            Toast.makeText(FacebookActivity.this,
                                    "Photo uploaded successfully",                                    
                                    Toast.LENGTH_LONG).show();                                                       
                        }                                               
                    });
            uploadRequest.executeAsync();
        } else {
            requestPermissions();
        }
    }
 
    public void postStatusMessage() {
        if (checkPermissions()) {
            Request request = Request.newStatusUpdateRequest(
                    Session.getActiveSession(), message,
                    new Request.Callback() {
                        @Override
                        public void onCompleted(Response response) {
                            if (response.getError() == null)
                                Toast.makeText(FacebookActivity.this,
                                        "Status updated successfully",
                                        Toast.LENGTH_LONG).show();
                        }
                    });
            request.executeAsync();
        } else {
            requestPermissions();
        }
    }
 
    public boolean checkPermissions() {
        Session s = Session.getActiveSession();
        if (s != null) {
            return s.getPermissions().contains("publish_actions");
        } else
            return false;
    }
 
    public void requestPermissions() {
        Session s = Session.getActiveSession();
        if (s != null)
            s.requestNewPublishPermissions(new Session.NewPermissionsRequest(
                    this, PERMISSIONS));
    }
 
    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
        buttonsEnabled(Session.getActiveSession().isOpened());
    }
 
    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }
 
    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }
 
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }
 
    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);        
        uiHelper.onSaveInstanceState(savedState);
    }
}
