package wazzatimagescanner;

import java.io.File;
import java.io.IOException;

import android.os.Environment;
import android.util.Log;



/**
 * @author nsinha
 *
 * WLCardScanner provides a set of methods which can be used to
 * recognize the data written on a credit card.
 */

public class WLImageScanner {


	/**
	 * Set of error codes to identify the return type from the native call;
	 */
	static final int NO_ERROR = 0;
	static final int FAILED_TO_SEGMENT = 1;
	static final int FAILED_TO_BOUND_DIGIT = 2;
	static final int FAILED_TO_RECOGNIZE_TEXT = 3;

	/**
	 * Instance of WLNative class which do actual native calls.
	 */
	private static WLNative mNativeInvoker = new WLNative();

	public static String mfinalDest;

	/**
	 * Scans the provided image matrix and returns the recognized digits.
	 * 
	 * @param width - Frame width of the image.
	 * @param height - Frame Height of the image
	 * @param frameData - Data matrix of the provided image. 
	 * @param bgra - It will get filled with the processed RGBA frame. To be used only if needed.
	 * @return Recognized text from the image.
	 * @throws WLAuthenticationException 
	 */
	protected static String scan(int width, int height, byte[] frameData, int[] bgra, boolean useLocation) throws WLAuthenticationException {

		if(!WLAuthenticate.isUserAuthenticated()){
			throw new WLAuthenticationException();
		}
		WLImageManager.getInstance().setLastProcessedImageData(frameData);
		
		int[] success = new int[] {-1,-1,-1,-1};
		String recText;
		double lat= -1;
		double lon = -1;
		if(useLocation){						
			lat = WLGeoLocationConstants.getInstance().getLatitude();
			lon = WLGeoLocationConstants.getInstance().getLongitude();
			Log.i("GeoLocaiton","Geo Location is set to true" + lat +":"+lon);
		}
		
		recText = mNativeInvoker.Search(width, height, frameData, bgra, success,lat,lon);

		if( success[NO_ERROR] != 0 ){
			//This means we have got some error while processing.
			switch (success[NO_ERROR]){
			case FAILED_TO_SEGMENT:
				System.out.println("Failed to segment the card for rectangle. Please try again!");
				recText="";
				break;
			case FAILED_TO_BOUND_DIGIT:
				System.out.println("Failed to segment the card for rectangle. Please try again!");
				recText="";
				break;
			case FAILED_TO_RECOGNIZE_TEXT:
				System.out.println("Failed to recognize the text. Please try again!");
				recText="";
				break;
			default:
				break;			
			}
		}

		return recText;
	}

	/**
	 * Scans the file from the provided path and returns the recognized digits.
	 * 
	 * @param width - Camera Preview width of the image.
	 * @param height - Camera Preview Height of the image
	 * @param frameData - Data matrix of the provided image. 
	 * @return
	 * @throws WLAuthenticationException 
	 */
	protected static String scan(int width, int height, byte[] frameData) throws WLAuthenticationException {
		int frameSize = width * height;
		int[] bgra = new int[frameSize];
		return scan(width, height, frameData, bgra, false);
	}
	
	/**
	 * Scans the file from the provided path and returns the recognized digits.
	 * 
	 * @param width - Camera Preview width of the image.
	 * @param height - Camera Preview Height of the image
	 * @param frameData - Data matrix of the provided image.
	 * @param  userLocation - Use geo location in calculating the data
	 * @return
	 * @throws WLAuthenticationException 
	 */
	protected static String scan(int width, int height, byte[] frameData, boolean useLocation) throws WLAuthenticationException {
		int frameSize = width * height;
		int[] bgra = new int[frameSize];
		return scan(width, height, frameData, bgra,useLocation);
	}


	/**
	 * Scans the file from the provided path and returns the recognized digits.
	 * @param path - Path where image is stored.
	 * @return Recognized Annotations
	 * @throws IOException - Thrown when the provided path is invalid
	 * @throws WLAuthenticationException 
	 */
	protected static String scan(String path) throws IOException, WLAuthenticationException {

		
		WLImageManager.getInstance().setLastProcessedImagePath(path);
		int[] success = new int[] {-1,-1,-1,-1};
		String recText;
		
		setFileStorageLocation();
		
		recText = mNativeInvoker.SearchPath(path,success);
		
		if( success[NO_ERROR] != 0 ){
			//This means we have got some error while processing.
			switch (success[NO_ERROR]){
			case FAILED_TO_SEGMENT:
				Log.i("WLLIB","Failed to segment the card for rectangle. Please try again!");
				recText="";
				break;
			case FAILED_TO_BOUND_DIGIT:
				Log.i("WLLIB","Failed to segment the card for rectangle. Please try again!");
				recText="";
				break;
			case FAILED_TO_RECOGNIZE_TEXT:
				Log.i("WLLIB","Failed to recognize the text. Please try again!");
				recText="";
				break;
			default:
				break;			
			}
		}
		return recText;
	}
	
	/**
	 * 
	 * @param width
	 * @param height
	 * @param frameData
	 * @param bgra
	 */
	protected static void findFeatures(int width, int height, byte frameData[], int[] bgra){
		mNativeInvoker.FindFeatures(width, height, frameData, bgra);
	}

	protected static void setFileStorageLocation(){
		
		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		String applicationName =  WLClientAcitivityInfo.getApplicationName();
		/* Preparing the directory  */
		File directory = new File(externalStorage+"/"+applicationName);
		directory.mkdirs();		
		mfinalDest = externalStorage+"/"+applicationName;		
		Log.i("WL",mfinalDest);
	}
	
}
