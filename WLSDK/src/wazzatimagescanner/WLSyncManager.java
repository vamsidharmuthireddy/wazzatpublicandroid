package wazzatimagescanner;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

public class WLSyncManager {

	/*Public*/
	public enum SYNC_MODE{
		UPLOAD_ONLY  ,
		DOWNLOAD_ONLY ,
		UPLOAD_AND_DOWNLOAD,
		NONE
	};


	/* Error Code
	 * 
	 */
	public static final int NO_ERROR = 0;
	public static final int DOWNLOAD_FAILED = 1;
	public static final int UPLOAD_FAILED = 2;
	public static final int DISCONNECTED= 3;
	/*Private*/
	String WL_SERVER_CHECK_URL = "http://www.wazzatlabs.com/checkIfSyncNeeded.php?";

	WLFileDownloader mDownloader = new  WLFileDownloader();

	WLFileUploader mUploader = new WLFileUploader();		

	boolean isLocalDataChanged = false;

	boolean isServerDataChanged = false;

	boolean isSyncCompleted = false;

	int mTempVersion = 0;

	SYNC_MODE mSyncMode = SYNC_MODE.UPLOAD_AND_DOWNLOAD;

	int mErrorCode = NO_ERROR;

	WLSyncStatusListener mSyncListener;


	public boolean checkIfSyncRequired(){
		boolean ret;
		ret = isLocalDataChanged = WLData.isLocalDataModified();

		if(checkServerDataModified()){
			isServerDataChanged = true;
			Log.i("Server Sync value", "server data has been changed" );
		}

		return ret || isServerDataChanged;
	}

	private boolean checkServerDataModified() {
		boolean ret = false;

		URL uri = null;
		HttpURLConnection urlConnection = null;
		try {
			String temp_url = WL_SERVER_CHECK_URL +  "token=" + WLClientAcitivityInfo.getToken() +"&version="+WLClientAcitivityInfo.getVersion();
			Log.i("Server Sync","URL" + temp_url);
			uri = new URL(temp_url);


			urlConnection = (HttpURLConnection) uri.openConnection();
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			String result = "";			

			if(in != null){
				BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(in));
				String line = "";
				while((line = bufferedReader.readLine()) != null)
					result += line;

				in.close();				
			}
			Log.i("Server Sync value", "Data result :" + result + ":" );
			if(! result.trim().equalsIgnoreCase("0")){
				mTempVersion= Integer.parseInt(result.trim());	
				ret = true;	
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();	
		}finally{

			urlConnection.disconnect();
		}


		return ret;		
	}

	private boolean isConnected(){
		ConnectivityManager cm =
				(ConnectivityManager)WLClientAcitivityInfo.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null &&
				activeNetwork.isConnectedOrConnecting();
		Log.i("Connect","not connected to anything " + Boolean.toString(isConnected));
		if(!isConnected){
			mErrorCode = DISCONNECTED;
		}
		return isConnected;
	}

	public WLSyncManager(SYNC_MODE mode,WLSyncStatusListener listener){
		mSyncMode = mode;
		mSyncListener = listener;
	}

	public WLSyncManager(WLSyncStatusListener listener){
		mSyncListener = listener;
	}	

	public void sync(){
		isSyncCompleted = false;		
		new AsyncTask<String, String, String>(){

			@Override
			protected String doInBackground(String... params) {



				if( !isConnected()){
					Log.i("WLSync","Not Conneced to internet!");
					isSyncCompleted = false;
					return null;
				}

				if( ! checkIfSyncRequired() ){
					isSyncCompleted = true;
					Log.i("WLSync","Already Updated!");
					return null;
				}

				boolean isUploadSuccess= true;
				boolean isDownloadSuccess= true;

				if( ( mSyncMode == SYNC_MODE.UPLOAD_ONLY || 
						mSyncMode == SYNC_MODE.UPLOAD_AND_DOWNLOAD) &&
						isLocalDataChanged ){
					if(mUploader.upload()){
						isLocalDataChanged = false;
					}
					else{
						mErrorCode = UPLOAD_FAILED;
						isUploadSuccess =false;
					}
				}
				if( ( mSyncMode == SYNC_MODE.DOWNLOAD_ONLY || 
						mSyncMode == SYNC_MODE.UPLOAD_AND_DOWNLOAD) &&
						isServerDataChanged ){
					if(mDownloader.startDownloading(null)){
						isServerDataChanged = false;

						WLSharedPreference sharedPref = WLSharedPreference.getInstance();
						
						Log.i("WLSync","The Current version of the data is :" + mTempVersion);
						
						sharedPref.setVersionNumber(mTempVersion);
						
						//New files are here now. Re load

						Log.i("DataLoader","Loading data....");			
						if( !WLData.loadData() ){
							Log.e("DataLoder","Data load failed");
						}
						else{
							Log.i("DataLoader","Data load completed");
						}

						mErrorCode = DOWNLOAD_FAILED;  // This may overwrite the previous error message over.
					}
					else{
						isDownloadSuccess = false;
					}
				}									

				if(isUploadSuccess && isDownloadSuccess ){
					isSyncCompleted = true;
				}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			if(isSyncCompleted){	
				mSyncListener.onSyncSuccess();

			}else{
				mSyncListener.onSyncFailure( mErrorCode );
			}
		}

		@Override
		protected void onProgressUpdate(String... values) {
			mSyncListener.whileSyncing();
		}
		
		protected void onPreExecute() {
			Log.i("WLSync","Initiating the synchronization");
			mSyncListener.preSyncTask();
		};

	}.execute("");

}

}
