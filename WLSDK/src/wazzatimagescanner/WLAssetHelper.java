package wazzatimagescanner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.content.res.AssetManager;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;


/***
 * Manages all the android assets related jobs.
 * This class is responsible for reading and extracting the asset data.
 * 
 * @author wazzatLabs
 *
 */
public class WLAssetHelper {
	
	static final ArrayList<String> FILE_LISTS = new ArrayList<String>(){

		private static final long serialVersionUID = 1L;

		{		
			add("Annotations.txt");
			add("DCount.txt");
			add("HKMeans_10000.Tree");
			add("InvertedIndex.txt");
			add("InvertedIndex_GV.txt");
			add("NumImages.txt");
			add("Objects.txt");
			add("Locations.txt");
			add("ObjectsChild.txt");
		}
	};
	
	
	AssetManager mAssetManager = WLClientAcitivityInfo.getContext().getAssets();
	
	int mProgressStatus= 30;

	Handler mHandler = new Handler();
	
	ProgressBar mProgressBar = null;
	
	// Progress Status is revognized our of 100
	private void updateProgressBar(final int progresStatus){

		if(mHandler !=null  && mProgressBar !=null){
			mHandler.post(new Runnable() {
				public void run() {
					mProgressBar.setProgress(progresStatus);					
				}
			});
		}
	}
	
	/**
	 * Extracts the assets file to external storage path.
	 * 
	 * @return
	 */
	public boolean startCopy(){
		return startCopy(null);
	}
	
	/**
	 * Extracts the assets file to external storage path.
	 * 
	 * @param progress Bar - Updates the status of progress 
	 * @return true on success otherwise false
	 */
	public boolean startCopy(ProgressBar pb){
		mProgressBar = pb;
		boolean ret = false;
		
		String externalStorage = Environment.getExternalStorageDirectory().getAbsolutePath();
		String applicationName =  WLClientAcitivityInfo.getApplicationName();
		String storagePath = externalStorage + File.separator + applicationName + File.separator;
		/* Create directoty for data files  to store*/

		File directory = new File(externalStorage+"/"+applicationName);
		
		if( !directory.mkdirs()  && !directory.isDirectory() ){
			Log.i("AssetManager","Failed to make directory: " + externalStorage+"/"+applicationName);
			return false;
		}
		
		
		for(int i = 0 ;  i < FILE_LISTS.size() ; i++){
			try {				
				InputStream in = mAssetManager.open(FILE_LISTS.get(i));
				String fileName = storagePath + FILE_LISTS.get(i);
				Log.i("AssetManager","Starting to unpack File:"+fileName);
				File file = new File(fileName);
				if (!file.exists()) {
				        try {
				            file.createNewFile();
				        } catch (IOException e) {
				            e.printStackTrace();
				        }
				}
				FileOutputStream out = new FileOutputStream(fileName);
				
				byte[] buffer = new byte[1024];
			    int read;
			    while((read = in.read(buffer)) != -1){
			            out.write(buffer, 0, read);
			    }
			    in.close();
			    out.flush();
			    out.close();
			    
			    ret = true;
			    updateProgressBar(mProgressStatus);
				mProgressStatus+=5;
				Log.i("AssetManager","Starting to unpack Done:"+fileName);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/* Create directoty for thumbs to store*/
		 directory = new File(externalStorage+"/"+applicationName+"/thumbs");
		directory.mkdirs();
		
		return ret;
	}
	
}
