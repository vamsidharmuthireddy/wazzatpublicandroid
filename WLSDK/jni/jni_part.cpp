#include <jni.h>
#include <vector>

#include <stdio.h>
#include <algorithm>
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <map>
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <fstream>
#include <android/log.h>


using namespace std;
using namespace cv;

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))

typedef struct Tree{
	int centers[10][128];
	struct Tree* sub[10];
	int nodes[10];
	int n_centers;
	int n_sub;
	int depth;
}Tree;


typedef struct RImage{
	int index;
	double r_score;
	double m_score;
	vector< Point2f > corners;
}RImage;

typedef struct obj{
	string name;
	double score;
}Object;

Tree* ParseTree(ifstream &t_file, int d){
	if( !t_file.good() ){
		return NULL;
	}
	int centers, subs;
	t_file >> centers >> subs;
	Tree *t = (Tree*)malloc(sizeof(Tree));
	t->n_centers = centers;
	t->n_sub = subs;
	t->depth = d;
	for(int i = 0 ; i < centers ; i++ ){
		for(int j = 0 ; j < 128 ; j++ ){
			t_file >> t->centers[i][j];
		}
	}
	for(int i = 0 ; i < subs ; i++ ){
		t->sub[i] = ParseTree(t_file,d+1);
	}
	for(int i = subs ; i < 10 ; i++){
		t->sub[i] = NULL;
	}
	return t;
}

void GetPath(Tree* tree , int* center , int* path , int depth){
	if(tree == NULL){
		path[depth] = -1;
		return;
	}
	double minD = 100000;
	double tempD;
	int minC = 2;
	for(int i = 0 ; i < tree->n_centers ; i++ ){
		tempD = 0;
		for(int j = 0 ; j < 128 ; j++ ){
			tempD += pow((double)(tree->centers[i][j] - center[j]),2.0);
		}
		tempD = sqrt(tempD);
		if(tempD < minD){
			minD = tempD;
			minC = i;
		}
	}
	path[depth] = minC+1;
	GetPath(tree->sub[minC], center, path, depth+1);
}

int GetWord( int *path ){
	int w = 0;
	for( int i = 0 ; i < 3 ; i++ ){
		//		cout << path[i] << " ";
		w = (w*10) + path[i] - 1;
	}
	w += 1;
	//	cout << w << endl;
	return w;
}


int compare ( const void *p1 , const void *p2 ){
	double p1_c = (* (RImage *)p1).r_score;
	double p2_c = (* (RImage *)p2).r_score;
	return p1_c - p2_c < 0 ? 1 : -1;
}

int compare2 ( const void *p1 , const void *p2 ){
	double p1_c = (* (Object *)p1).score;
	double p2_c = (* (Object *)p2).score;
	return p1_c - p2_c < 0 ? 1 : -1;
}


int isInside ( int x , int y , int x1 , int y1  , int x3 , int y3 )
{
	if ( x >= x1 && x <= x3 ){
		if ( y >= y1 && y <= y3 ){
			return 1 ;
		}
	}
	return 0;
}

int findInt ( string c)
{
	int a = 0 ;
	int i = 0 ;
	while ( c[5+i] != '.' )
	{
		a = a*10 + (int)(c[5 + i] - '0') ;
		i++;
	}
	return a ;
}


int N,N_orig;
map< int, map< int, int > > InvertedFile;
Tree *tree;
vector < string > ImageList;
vector < string > Annotations;
vector < int > TF;
map< int, int > QueryHist;
vector < int > QWords;
int O;
map< string, int > Objects;
map<string, vector<string> > ObjectParts;



extern "C" {
JNIEXPORT void JNICALL Java_com_wazzatcarz_common_CVLoadTask_LoadData(JNIEnv* env, jobject thiz)
{
	//=======================================
	//	Loading #images in database
	//=======================================
	ifstream NumImagesFile("/sdcard/Carz/NumImages.txt");
	LOGI("ProgressCheck: NumImages open status: %d", NumImagesFile.is_open());
	NumImagesFile >> N;
	N_orig = N;
	NumImagesFile.close();

	//=======================================
	//	Loading distinct objects in database
	//=======================================
	ifstream ObjectsFile("/sdcard/Carz/Objects.txt");
	string tempobj, temppart; int tempcount;
	LOGI("ProgressCheck: Objects open status: %d", ObjectsFile.is_open());
	ObjectsFile >> O;
	for( int i = 0 ; i < O ; i++ ){
		ObjectsFile >> tempcount >> tempobj;
		Objects[tempobj] = tempcount;
	}
	ObjectsFile.close();

	//=======================================
	//	Loading distinct objects in database
	//=======================================
	ifstream ObjectPartsFile("/sdcard/Carz/ObjectParts.txt");
	LOGI("ProgressCheck: Objects open status: %d", ObjectPartsFile.is_open());
	while( ObjectPartsFile.good() && !ObjectPartsFile.eof() ){
		ObjectPartsFile >> tempobj >> tempcount;
		getline(ObjectPartsFile, temppart);
		if( ObjectPartsFile.bad() || ObjectPartsFile.eof() || !ObjectPartsFile.good() ) break;
		for( int i = 0 ; i < tempcount ; i++ ){
			getline(ObjectPartsFile, temppart);
			ObjectParts[tempobj].push_back( temppart );
		}
	}


	//=======================================
	//	Loading Inverted Index File
	//=======================================
	ifstream InvertedIndexFile("/sdcard/Carz/InvertedIndex.txt",ios::in);

	LOGI("ProgressCheck: InvertedFile open status: %d", InvertedIndexFile.is_open());
	int vword, vcount, vimage, vnum;
	while(InvertedIndexFile.good()){
		InvertedIndexFile >> vword >> vcount;
		for(int i = 0 ; i < vcount ; i++ ){
			InvertedIndexFile >> vimage >> vnum ;
			InvertedFile[vword][vimage] = vnum;
		}
	}
	InvertedIndexFile.close();
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","InvertedIndexFile Loaded");
	LOGI("ProgressCheck: InvertedFile size= %d",(int)InvertedFile.size());

	//=======================================
	//	Loading the HKMeans Tree
	//=======================================
	ifstream t_file("/sdcard/Carz/HKMeans_1000.Tree",ios::in);
	LOGI("ProgressCheck: Tree file open status: %d", t_file.is_open());
	tree = ParseTree(t_file,0);
	t_file.close();
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","HKMeans tree Loaded");

	//======================================================
	//	Loading Image file names and Term Frequency Count
	//======================================================
	string imageListFileName = "/sdcard/Carz/Annotations.txt";
	string temp, tempLine;
	ifstream imageListFile;
	imageListFile.open(imageListFileName.c_str(),ios::in);
	string TF_filename = "/sdcard/Carz/DCount.txt";
	int tempTF;
	ifstream TF_file;
	TF_file.open(TF_filename.c_str(), ios::in );

	for(int i = 1 ; i <= N ; i++ ){
		imageListFile >> temp;
		getline( imageListFile, tempLine);
		ImageList.push_back( temp );
		Annotations.push_back( tempLine );
		TF_file >> tempTF;
		TF.push_back(tempTF);

	}
	//=======================================
			//	Loading Image file names
			//=======================================
	/*         ifstream an1_file("/sdcard/Golkonda_5500/AnnInfo.txt",ios::in);
         ifstream an2_file("/sdcard/Golkonda_5500/AnnText.txt",ios::in);
	     ifstream an3_file("/sdcard/Golkonda_5500/AnnBoundary.txt",ios::in);
	     int TotalAnn;
	     an1_file>>TotalAnn;
	     string TempImageId;
	     for( int i = 0 ; i < TotalAnn ; i++ ){
	            Annotation TempAnn;
	            an1_file >> TempImageId >> TempAnn.type ;
	            getline( an2_file , TempAnn.text );
	            if( TempAnn.type.compare("OBJECT") == 0 ){
	                   an3_file >> TempAnn.boundary;
	            }
	            ImageAnns[ TempImageId ].push_back(TempAnn);
	     }
	     an1_file.close();
	     an2_file.close();
	     an3_file.close();

	 */
}

}

extern "C" {
JNIEXPORT jstring JNICALL Java_com_wazzatcarz_activity_Sample3View_Search(JNIEnv* env, jobject thiz, jint width, jint height, jbyteArray yuv, jintArray bgra)
//JNIEXPORT jstring JNICALL Java_com_example_wazzatcarz_QueryActivity_Search(JNIEnv* env, jobject thiz)
{

/* PRE pROCESSING */
	QWords.clear();
	QueryHist.clear();
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Start...");
/* PRE pROCESSING ends*/
	//=======================================
	//	Loading the frame
	//=======================================
	//		Mat* pMatGr=(Mat*)addrGray;
	//	    Mat* pMatRgb=(Mat*)addrRgba;

	//		Mat img_temp = *pMatGr;
	//		Mat img_temp = imread("/data/data/com.example.heritagecam/files/TestImage.jpg",CV_LOAD_IMAGE_GRAYSCALE);
	
	
	
	/* Take and Process Photo STARTS */
	

    jbyte* _yuv  = env->GetByteArrayElements(yuv, 0);
    jint*  _bgra = env->GetIntArrayElements(bgra, 0);

    Mat myuv(height + height/2, width, CV_8UC1, (unsigned char *)_yuv);
    Mat mbgra(height, width, CV_8UC4, (unsigned char *)_bgra);
    Mat img_temp(height, width, CV_8UC1, (unsigned char *)_yuv);

    cvtColor(myuv, mbgra, CV_YUV420sp2BGR, 4);


/*	Mat img_temp = imread("/sdcard/TestImage.jpg",CV_LOAD_IMAGE_GRAYSCALE);

	int height = img_temp.rows;
	int width = img_temp.cols;
*/

	Mat img;
	if( height > 400 || width > 400 ){
		int new_h = 250;
		int new_w = (new_h*width)/height;
		img.create(new_h,new_w,CV_8UC3);
		resize(img_temp,img,img.size(),0,0, INTER_LINEAR);
	}else{
		img = img_temp.clone();
	}

	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Frame Loaded");
	/* Take and Process Photo ENDS  */

	//#if 0
	//============================================
			//	Feature Extraction and Histogram Building starts
	//============================================
	SiftFeatureDetector detector;
	std::vector<KeyPoint> keypoints;

	SiftDescriptorExtractor extractor;
	Mat descriptors;

	detector.detect(img,keypoints);
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","SIFT detection");

	extractor.compute(img,keypoints,descriptors);
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","SIFT descriptor extraction");

		//	Feature Extraction and Histogram Building ends
		
		
/* QUANTIZE STARTS */
	int des[128];
	for(int i = 0 ; i < descriptors.rows ; i++ ){
		for(int j = 0 ; j < 128 ; j++){
			des[j] = descriptors.at<float>(i,j);
		}
		int path[12];
		GetPath( tree, des, path, 0 );
		int hist = GetWord( path );
		//		cout << hist << " ";
		if(QueryHist.count( hist ) > 0){
			QueryHist[ hist ] += 1;
		}
		else{
			QueryHist[ hist ] = 1;
		}
		QWords.push_back( hist );

	}
	
/* QUANTIZE ends */	
	//	cout << endl << QWords.size() << endl;

	/* search starts*/
	
	
	double idf;
	RImage *ImagesRetrieved = new RImage[N+5];
	for(int i = 1 ; i <= N ; i++ ) {
		ImagesRetrieved[ i ].index  = i;
		ImagesRetrieved[ i ].r_score  = 0;
		ImagesRetrieved[ i ].m_score  = 0;
	}
	int word, count;
	for( map< int, int >::iterator it = QueryHist.begin() ; it != QueryHist.end() ; ++it){
		word = (*it).first;
		count = (*it).second;
		if( InvertedFile[ word ].size() == 0 )
			continue;
		idf = log10( N/InvertedFile[word].size() );
		for( map< int , int >::iterator it2 = InvertedFile[word].begin() ; it2 != InvertedFile[word].end() ; ++it2 ){
			int terms = TF[ (*it2).first-1 ] > QWords.size() ? TF[ (*it2).first-1 ] : QWords.size() ;
			ImagesRetrieved[(*it2).first].r_score += ( ( ( count > (*it2).second ? (*it2).second : count ) * 1.0 )/terms)  * idf;
			//			ImagesRetrieved[(*it2).first].r_score += count * idf;
		}
	}
	qsort( ImagesRetrieved + 1, N , sizeof(ImagesRetrieved[ 1 ]), compare );

	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Inverted index search");

	/* search ends*/
	
/*	for( int n = 1 ; n <= 10 ; n++ ){
		__android_log_write( ANDROID_LOG_VERBOSE,"Annotation",ImageList[ ImagesRetrieved[n].index - 1 ].c_str() );
	}
*/


	/* GEOMETRICC VERFICATION STARTS */
	/* GEOMETRICC VERFICATION ENDS */

	
	/* Annotation extraction STARTS */
	
	
	string ref = ImageList[ ImagesRetrieved[1].index - 1 ];
	__android_log_write( ANDROID_LOG_DEBUG,"Progress","Annotating");

//	string check = ref.substr( 0, ref.find_last_of("_") - 1 );
//	int countCheck = 0;
//	__android_log_write( ANDROID_LOG_DEBUG,"Progress","Annotating");
	map<string, int> componentScores;
	Object compTopTen[20]; int countCompTopTen = 0;
	for( int n = 1 ; n <= 10 ; n++ ){
		__android_log_print( ANDROID_LOG_DEBUG,"Annotation","%s: %lf : %s",
				ImageList[ ImagesRetrieved[n].index - 1 ].c_str(), ImagesRetrieved[n].r_score,
				Annotations[ImagesRetrieved[n].index -1].c_str() );
		ref = ImageList[ ImagesRetrieved[n].index - 1 ];
		string tempComp = ref.substr(0, ref.find_last_of("_") );
		if(  componentScores.count(tempComp) > 0 ){
			compTopTen[ componentScores[tempComp] ].score+=ImagesRetrieved[n].r_score;
		}else{
			countCompTopTen++;
			componentScores[tempComp] = countCompTopTen;
			compTopTen[ countCompTopTen ].name = tempComp;
			compTopTen[ countCompTopTen ].score = ImagesRetrieved[n].r_score;
		}
	}
	qsort(compTopTen, countCompTopTen, sizeof(compTopTen[0]), compare2);
	__android_log_write( ANDROID_LOG_DEBUG,"Progress","Annotating");


	string result="";
	for( int i = 0 ; i <= min(countCompTopTen,4) ; i++ ){
		result = result + compTopTen[i].name + ":";
	}

/* annotation extraction ends */

/* Send data back */
    env->ReleaseIntArrayElements(bgra, _bgra, 0);
    env->ReleaseByteArrayElements(yuv, _yuv, 0);
	__android_log_print( ANDROID_LOG_DEBUG,"AnnotationFinal","%s",result.c_str() );
	return env->NewStringUTF(result.c_str());

	//#endif
	//	   return env->NewStringUTF("hello.. debugging!");

/* Send data back */
	/*
       if( Aflag == 0 ){
    	   __android_log_write(ANDROID_LOG_VERBOSE,"Result","No Annotation Found");
//   		   putText(img, "No Annotation Found" , Point(20,100), FONT_HERSHEY_SIMPLEX, 1, Scalar(255,0,0,255), 1,8);
   		   char ret[100] = "No Annotation Found";

   		   return env->NewStringUTF(ret);
//           circle(*pMatRgb, Point(20,20), 10, Scalar(255,0,0,255));

       }
	 */
	//       imwrite("/sdcard/test_output.jpg",img);


}



}


extern "C" {
JNIEXPORT void JNICALL Java_com_wazzatcarz_activity_Sample3View_FindFeatures(JNIEnv* env, jobject thiz, jint width, jint height, jbyteArray yuv, jintArray bgra)
{
    jbyte* _yuv  = env->GetByteArrayElements(yuv, 0);
    jint*  _bgra = env->GetIntArrayElements(bgra, 0);

    Mat myuv(height + height/2, width, CV_8UC1, (unsigned char *)_yuv);
    Mat mbgra(height, width, CV_8UC4, (unsigned char *)_bgra);
    Mat mgray(height, width, CV_8UC1, (unsigned char *)_yuv);

    //Please make attention about BGRA byte order
    //ARGB stored in java as int array becomes BGRA at native level
    cvtColor(myuv, mbgra, CV_YUV420sp2BGR, 4);


    env->ReleaseIntArrayElements(bgra, _bgra, 0);
    env->ReleaseByteArrayElements(yuv, _yuv, 0);
}

}

extern "C" {
JNIEXPORT jstring JNICALL Java_com_wazzatcarz_common_CVLoadTask_UpdateData(JNIEnv* env, jobject thiz, jstring name)
{

	LOGI("Native Activity","here");

    const char *imgName = env->GetStringUTFChars( name, 0);
    if( Objects.count(imgName) > 0 ){
    	Objects[imgName]++;
    }else{
    	Objects[imgName] = 1;
    	O++;
    }
    char imgNamePadded[100];
    sprintf(imgNamePadded,"%s_%04d",imgName,Objects[imgName]);
	N++;
	Annotations.push_back(imgName);
	ImageList.push_back(imgNamePadded);
	TF.push_back( QWords.size() );

	for( map<int,int>::iterator it = QueryHist.begin() ; it != QueryHist.end() ; ++it ){
         InvertedFile[ it->first ][N] = it->second;
	}

/*

*/
	env->ReleaseStringUTFChars( name, imgName);
	return env->NewStringUTF(imgNamePadded);
}
}

extern "C" {
JNIEXPORT void JNICALL Java_com_wazzatcarz_common_CVLoadTask_UpdateDataLocal(JNIEnv* env, jobject thiz)
{

	ofstream InvertedIndexFile("/sdcard/Carz/InvertedIndex.txt",ios::out);
	    for( map<int, map<int, int > >::iterator it = InvertedFile.begin() ; it != InvertedFile.end() ; ++it ){
	         InvertedIndexFile << it->first << " " << (it->second).size() << endl;
	         for( map<int, int>::iterator jt = InvertedFile[it->first].begin() ; jt!= InvertedFile[it->first].end() ; ++jt ){
	        	 InvertedIndexFile << jt->first << " " << jt->second << endl;
	         }
	    }
	    InvertedIndexFile.close();
	    LOGI("Native activity","Inverted Index Updated");

	    ofstream AnnotationsFile("/sdcard/Carz/Annotations.txt",ios::out | ios::app);
		ofstream DCountFile("/sdcard/Carz/DCount.txt",ios::out | ios::app);
	    for( int i = N_orig ; i < N ; i++ ){
	    	AnnotationsFile <<  ImageList[i] << "\t" << Annotations[i]<< endl;
			DCountFile << TF[i] << endl;
	    }
		AnnotationsFile.close();
		DCountFile.close();

		ofstream NumImagesFile("/sdcard/Carz/NumImages.txt",ios::out);
		NumImagesFile << N << endl;
		DCountFile.close();

		//=======================================
		//	Loading distinct objects in database
		//=======================================
		ofstream ObjectsFile("/sdcard/Carz/Objects.txt");
		string tempobj; int tempcount;
		LOGI("ProgressCheck: Objects open status: %d", ObjectsFile.is_open());
		ObjectsFile << O << endl;
		for( map<string, int>::iterator it = Objects.begin(); it != Objects.end() ; ++it ){
			ObjectsFile << it->second << " " << it->first << endl;
		}
		ObjectsFile.close();
}
}

extern "C" {
JNIEXPORT jobjectArray JNICALL Java_com_wazzatcarz_activity_ResultActivity_GetObjectsList(JNIEnv* env, jobject thiz)
{
	LOGI("NativeActivity","to return object list");
	jobjectArray ret = env->NewObjectArray(Objects.size(), env->FindClass("java/lang/String"),0);
	LOGI("NativeActivity","giving array of objects as strings");
	int i=0;
	for( map<string, int>::iterator it = Objects.begin(); it != Objects.end() ; ++it, i++ ){
		env->SetObjectArrayElement(ret, i, env->NewStringUTF((it->first).c_str()) );
	}
	return ret;

}
}

extern "C" {
JNIEXPORT jobjectArray JNICALL Java_com_wazzatcarz_activity_ResultActivity_GetObjectPartsList(JNIEnv* env, jobject thiz, jstring name)
{
	const char *imgName = env->GetStringUTFChars( name, 0);
	jobjectArray ret = (jobjectArray)env->NewObjectArray(ObjectParts[imgName].size(), env->FindClass("java/lang/String"),0);
	int i=0;
	if( ObjectParts.count(imgName) == 0 ){
		return NULL;
	}
	for( vector<string>::iterator it = ObjectParts[imgName].begin(); it != ObjectParts[imgName].end() ; ++it, i++ ){
		env->SetObjectArrayElement(ret, i, env->NewStringUTF((*it).c_str()) );
	}
	return ret;

}
}
