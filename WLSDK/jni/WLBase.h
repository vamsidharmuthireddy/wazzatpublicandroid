#ifndef _WL_BASE_H
#define _WL_BASE_H
#include <jni.h>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/ml/ml.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "WLDataStore.h"

typedef char byte;

enum process_state
{
  NOT_STARTED           = 0,
  INITIATING            = 1,
  INITIATED             = 2,
  PREPROCESS_IMAGE      = 3,
  SEGMENTATION          = 4,
  FEATURE_EXTRACTION    = 5,
  QUANTIZE              = 6,
  SEARCH_FEATURE        = 7,
  GEOMETRIC_VERIFACTION = 8,
  ANNOTATION_CREATION   = 9,
  COMPLETED             = 10,
  VERIFY_RESULT         = 11,  
  COMPLETION_SUCCESS    = 12,
  COMPLETION_FAILURE    = 13  
};

class WLBase
{
	public :
		WLBase();

		~WLBase();

		bool init(bool useGV);
		
		void updatePackageInfo(string externalStorage, string packgeName);	
		
		std::string search( JNIEnv* env, jobject thiz, int width, int height, jbyte* yuv, jint* bgra ,jdouble latitude, jdouble longitude )	;
				
		void findFeatures( int width, int height, jbyte* yuv, jint* bgra);
		
		const char * updateObjectList(const char *name);
		
		void syncObjectListLocally();
		
		std::map<string, int >& getObjectList();
		
		vector<string> getChildObjectList(string &name);
		
		void setGeoLocationUseOn();

		void setGeoLocationUseOff();

		void toggleUseGV();
	
								
	protected :		
		void initprocessing();  /* This is the place where the processing sequence is defined. */				
		
	private :
		
		void resetStatus();	
		void process();	
		void updateState(process_state state);
		void updateNextState();   /* change the sequence of the process needs to be performed */				
		void computeWithOneImageonly();


		
		
		/* Following are the set of functions that we processed */
		void preProcessImage();
		void segment();
		void extractFeature();
		void quantize();
		void searchFeature();
		void doGeometricVerification();
		void createAnnotation();
		void verifyResult();
		double calculateGeoDistance(Location l1, Location l2);
		bool isDirectionValidated();

		process_state mCurrentState;		
		std::vector<process_state> mStateMachine;
		int mWidth;
		int mHeight;
		jbyte* mYuv;
		jint * mBgra;
		cv::Mat mProcessImg;
		cv::Mat mDescriptors;
		cv::Mat mLastProcessedImg;
		RImage *mImagesRetrieved;
		
		std::vector<KeyPoint> keypoints;
		WLDataStore *mDataStore;
		
		std::string mFinalResult;
		
		std::string mExternalStoragepath;
		std::string mPackageName;
		
		bool useGeoLocation;
		Location mLastCapturedLocation;
		JNIEnv* mJniEnv;
		jobject mJniObj;
		
};

#endif  // header gueard ends here
