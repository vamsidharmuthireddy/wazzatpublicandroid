#ifndef __WL_UTILTIY_H__
#define __WL_UTILTIY_H__

#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <map>
#include "WLStruct.h"

using namespace std;

inline Tree* parseTree(ifstream &t_file, int d){
	if( !t_file.good() ){
		return NULL;
	}
	int centers, subs;
	t_file >> centers >> subs;
	Tree *t = (Tree*)malloc(sizeof(Tree));
	t->n_centers = centers;
	t->n_sub = subs;
	t->depth = d;
	for(int i = 0 ; i < centers ; i++ ){
		for(int j = 0 ; j < 128 ; j++ ){
			t_file >> t->centers[i][j];
		}
	}
	for(int i = 0 ; i < subs ; i++ ){
		t->sub[i] = parseTree(t_file,d+1);
	}
	for(int i = subs ; i < 10 ; i++){
		t->sub[i] = NULL;
	}
	return t;
}


inline void GetPath(Tree* tree , int* center , int* path , int depth){
	if(tree == NULL){
		path[depth] = -1;
		return;
	}
	double minD = 100000;
	double tempD;
	int minC = 2;
	for(int i = 0 ; i < tree->n_centers ; i++ ){
		tempD = 0;
		for(int j = 0 ; j < 128 ; j++ ){
			tempD += pow((double)(tree->centers[i][j] - center[j]),2.0);
		}
		tempD = sqrt(tempD);
		if(tempD < minD){
			minD = tempD;
			minC = i;
		}
	}
	path[depth] = minC+1;
	GetPath(tree->sub[minC], center, path, depth+1);
}

inline int GetWord( int *path ){
	int w = 0;
	for( int i = 0 ; i < 4 ; i++ ){
		w = (w*10) + path[i] - 1;
	}
	w += 1;
	return w;
}

inline int compareRImage ( const void *p1 , const void *p2 ){
	double p1_c = (* (RImage *)p1).r_score;
	double p2_c = (* (RImage *)p2).r_score;
	return p1_c - p2_c < 0 ? 1 : -1;
}

inline int compareMImage ( const void *p1 , const void *p2 ){
	double p1_c = (* (RImage *)p1).m_score;
	double p2_c = (* (RImage *)p2).m_score;
	return p1_c - p2_c < 0 ? 1 : -1;
}

inline int compareObject ( const void *p1 , const void *p2 ){
	double p1_c = (* (Object *)p1).score;
	double p2_c = (* (Object *)p2).score;
	return p1_c - p2_c < 0 ? 1 : -1;
}

inline int isInside ( int x , int y , int x1 , int y1  , int x3 , int y3 )
{
	if ( x >= x1 && x <= x3 ){
		if ( y >= y1 && y <= y3 ){
			return 1 ;
		}
	}
	return 0;
}

inline int findInt ( std::string c )
{
	int a = 0 ;
	int i = 0 ;
	while ( c[5+i] != '.' )
	{
		a = a*10 + (int)(c[5 + i] - '0') ;
		i++;
	}
	return a ;
}



#endif //header guard ends here

