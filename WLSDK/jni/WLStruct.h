#ifndef __WL_STRUCT_H__
#define __WL_STRUCT_H__

#include <string>
#include <iostream>
#include "opencv2/core/core.hpp"
#include <android/log.h>

#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "native-activity", __VA_ARGS__))


using namespace std;

typedef struct Tree{
	int centers[10][128];
	struct Tree* sub[10];
	int nodes[10];
	int n_centers;
	int n_sub;
	int depth;
}Tree;


typedef struct RImage{
	int index;
	double r_score;
	double m_score;
	vector< cv::Point2f > corners;
}RImage;

typedef struct obj{
	string name;
	double score;
}Object;

typedef struct loc{
	double lat;
	double lon; 
}Location;

#endif //header guard ends here
