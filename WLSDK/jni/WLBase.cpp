#include "WLUtility.h"
#include "WLBase.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>

using namespace std;
using namespace cv;

WLBase::WLBase():
mWidth(640),
mHeight(480),
useGeoLocation(false),
mFinalResult("None")
{
	Location temp;
	temp.lat=temp.lon=-1;
	mLastCapturedLocation=temp;
	mStateMachine.push_back(INITIATING);
}

WLBase::~WLBase()
{
	//delete mDataStore;
}

bool WLBase::init(bool useGV)
{
	mDataStore = WLDataStore::getInstance(mExternalStoragepath,  mPackageName);
	mDataStore->performGV = useGV;
	
	mDataStore->initData();
	
	bool ret = false;
	if( mDataStore->isDataReady() ){
		ret = true;
	}
	return ret;
}			

void WLBase::updatePackageInfo(string externalStorage, string packgeName)
{
	mExternalStoragepath = externalStorage;
	mPackageName = packgeName;
}
	
string WLBase::search( JNIEnv* env, jobject thiz,int width, int height, jbyte* yuv, jint* bgra ,jdouble latitude, jdouble longitude)
{
	mJniObj = thiz;
	mJniEnv = env;

	mCurrentState = NOT_STARTED;
	mWidth = width;
	mHeight = height;
	mYuv = yuv;
	//mYuv = env->GetByteArrayElements(yuv, 0);
	mBgra = bgra;
	
	//jsize yuv_count = env -> GetArrayLength(yuv); 
	
	//__android_log_print(ANDROID_LOG_DEBUG,"Progress","YUV : %d",yuv_count);	
	
	mFinalResult = "NULL";
	
	if( latitude  >= 0.0 &&
		longitude >= 0.0 ){
		__android_log_print(ANDROID_LOG_DEBUG,"Progress","YUV : Geo-location is set to true %0.5f %0.5f",latitude,longitude);	
		useGeoLocation = true;
		Location temp;
		temp.lat = latitude;
		temp.lon = longitude;
		mLastCapturedLocation = temp;
	}
	
	process();
	
	return mFinalResult;
}

void WLBase::computeWithOneImageonly(){

}

void WLBase::findFeatures( int width, int height, jbyte* yuv, jint* bgra)
{
	Mat myuv(height + height/2, width, CV_8UC1, (unsigned char *)yuv);
	Mat mbgra(height, width, CV_8UC4, (unsigned char *)bgra);
	Mat mgray(height, width, CV_8UC1, (unsigned char *)yuv);

	//Please make attention about BGRA byte order
	//ARGB stored in java as int array becomes BGRA at native level
	cvtColor(myuv, mbgra, CV_YUV420sp2BGR, 4);

}

const char * WLBase::updateObjectList(const char *name)
{
	__android_log_write(ANDROID_LOG_VERBOSE,"native activity","Upload Data ");
	
	const char *imgName = name;
	
	if( (mDataStore->Objects).count(imgName) > 0 ){
		(mDataStore->Objects)[imgName]++;
	}else{
		(mDataStore->Objects)[imgName] = 1;
		(mDataStore->LocationList)[imgName] = mLastCapturedLocation;
		(mDataStore->O)++;
	}
	char imgNamePadded[100];
	sprintf(imgNamePadded,"%s_%05d",imgName,(mDataStore->Objects)[imgName]);
	
	LOGI("ProgressCheck: Value of N:%d", (mDataStore->N));
	(mDataStore->N)++;
	LOGI("ProgressCheck: Value of N:%d\nValue of annotation %d", (mDataStore->N),(mDataStore->Annotations).size());
	(mDataStore->Annotations).push_back(imgName);
	LOGI("ProgressCheck: Value of Annotation:%d\nValue of ImageList %d", (mDataStore->Annotations).size(),(mDataStore->ImageList).size());
	(mDataStore->ImageList).push_back(imgNamePadded);
	LOGI("ProgressCheck: Value of image list:%d\nValue of TFt %d", (mDataStore->ImageList).size(),(mDataStore->TF).size());	
	(mDataStore->TF).push_back( (mDataStore->QWords).size() );
	LOGI("ProgressCheck: Value of  TFt %d %d", (mDataStore->TF).size(),(mDataStore->QWords).size());	

	for( map<int,int>::iterator it = (mDataStore->QueryHist).begin() ; it != (mDataStore->QueryHist).end() ; ++it ){
		(mDataStore->InvertedFile)[ it->first ][mDataStore->N] = it->second;
		LOGI("ProgressCheck: Updating inverted file %d %d",it->first,it->second);			
	}
	__android_log_write(ANDROID_LOG_VERBOSE,"native activity","Upload Data Done");
	string thumb_name = name;
	string thumb_path = mExternalStoragepath +"/"+	mPackageName +"/thumbs/" +  imgNamePadded +".jpg";
	ifstream file(thumb_path.c_str());
    if(!file){
		LOGI("ProgressCheck: Saving the thumbnail : %s",thumb_path.c_str());			
		imwrite(thumb_path.c_str(),mLastProcessedImg);
    }
    
	return imgNamePadded;
}

void WLBase::syncObjectListLocally()
{
	mDataStore -> persistDataLocally();
}

map<string, int >& WLBase::getObjectList()
{
	return (mDataStore->Objects);
}

vector<string> WLBase::getChildObjectList(string &name)
{
	vector<string> temp;	
	if( (mDataStore->objectsChildList).find(name) != (mDataStore->objectsChildList).end() ){
		__android_log_print( ANDROID_LOG_DEBUG,"Progress","Child Found");
		temp = (mDataStore->objectsChildList)[name];
	}
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","All set returning");

	return temp;
}


/* Override this to change the sequence number */
/* Fill up the state machine in the stack */
/* First item in the queue is INITIATING which calls the init processing functions */

void WLBase::initprocessing()
{
	(mDataStore->QWords).clear();
	(mDataStore->QueryHist).clear();
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Start...");

	// NOTE: INITIATING is already added in the constructor
	// NOTE: Add Completion success/failure at the end of the state.
	mStateMachine.push_back(PREPROCESS_IMAGE);
	mStateMachine.push_back(FEATURE_EXTRACTION);
	mStateMachine.push_back(QUANTIZE);
	mStateMachine.push_back(SEARCH_FEATURE);
	mStateMachine.push_back(GEOMETRIC_VERIFACTION);
	mStateMachine.push_back(ANNOTATION_CREATION);
	mStateMachine.push_back(COMPLETED);
	mStateMachine.push_back(VERIFY_RESULT);

	updateNextState();
}

void WLBase::preProcessImage()
{
	Mat myuv(mHeight + mHeight/2, mWidth, CV_8UC1, (unsigned char *)mYuv);
    Mat mbgra(mHeight, mWidth, CV_8UC4, (unsigned char *) mBgra);
    Mat img_temp(mHeight, mWidth, CV_8UC1, (unsigned char *)mYuv);

	mLastProcessedImg = img_temp;
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","lAST PROCESSED IMAGE UPDATED");
	__android_log_print( ANDROID_LOG_DEBUG,"Progress","Width:%d and Heigh:%d",mWidth,mHeight);
	
    cvtColor(myuv, mbgra, CV_YUV420sp2BGR, 4);	
	
	__android_log_print( ANDROID_LOG_DEBUG,"Progress","Width:%d and Heigh:%d",mWidth,mHeight);
	Mat img;
	if( mHeight > 700 || mWidth > 700 ){
		int new_h = 480;
		int new_w = (new_h*mWidth)/mHeight;
		img.create(new_h,new_w,CV_8UC3);
		resize(img_temp,img,img.size(),0,0, INTER_LINEAR);
	}else{
		img = img_temp.clone();
	}
	
	imwrite("/sdcard/temp.jpg",img );
	mProcessImg = img;
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Frame Loaded");
	updateNextState();
}


void WLBase::process()
{
	bool ifContinue = true;
	switch(mCurrentState){
	case NOT_STARTED :
		updateState(INITIATING);
		break;
	case INITIATING:
		initprocessing();
		break;
	case INITIATED :
		updateState(PREPROCESS_IMAGE);
		break;
	case PREPROCESS_IMAGE :
		preProcessImage();	
		break;
	case SEGMENTATION :
		segment();
		break;
	case FEATURE_EXTRACTION :
		extractFeature();
		break;
	case QUANTIZE :
		quantize();
		break;
	case SEARCH_FEATURE :
		searchFeature();
		break;
	case GEOMETRIC_VERIFACTION :
		doGeometricVerification();
		break;
	case ANNOTATION_CREATION :
		createAnnotation();
		break;
	case COMPLETED : 
		updateNextState();
		break;
	case VERIFY_RESULT : 
		verifyResult();
		break;
	case COMPLETION_SUCCESS : 
		ifContinue = false;
		break;
	case COMPLETION_FAILURE :
		ifContinue = false;
		resetStatus();
		break;
	default:
		break;		
	}
	if ( ifContinue ){
		process();
	}
}

void WLBase::updateNextState()
{
	vector<process_state>::iterator it =   mStateMachine.begin( );
	for( ; it < mStateMachine.end( ) ; ++it ){
		if( mCurrentState == *it ){
			break;
		}
	}
	if(  it != mStateMachine.end() ){
		/* We are at the end of the state machine. Setting the state to completion*/
		if( it+1 != mStateMachine.end() ){
			it++;
			mCurrentState = *it;
		}
		else{
			mCurrentState = COMPLETED;
		}
	}
	else{
		// The current state does not exist. Something is not right . Reestting the state
		resetStatus();
	}
	LOGI("State Update %d",mCurrentState);

}

void WLBase::updateState(process_state state)
{
	mCurrentState = state;
}

void WLBase::resetStatus()
{
	mCurrentState = NOT_STARTED;
}

void WLBase::segment(){
	updateNextState();
}

void WLBase::extractFeature(){

	SiftFeatureDetector detector;


	SiftDescriptorExtractor extractor;
	Mat descriptors;

	detector.detect(mProcessImg,keypoints);
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","SIFT detection");

	extractor.compute(mProcessImg,keypoints,descriptors);
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","SIFT descriptor extraction");
	mDescriptors = descriptors;
	updateNextState();
}

void WLBase::quantize(){
	int des[128];
	for(int i = 0 ; i < mDescriptors.rows ; i++ ){
		for(int j = 0 ; j < 128 ; j++){
			des[j] = mDescriptors.at<float>(i,j);
		}
		int path[12];
		GetPath( mDataStore->tree, des, path, 0 );
		int hist = GetWord( path );
		//		cout << hist << " ";
		if((mDataStore->QueryHist).count( hist ) > 0){
			(mDataStore->QueryHist)[ hist ] += 1;
		}
		else{
			(mDataStore->QueryHist)[ hist ] = 1;
		}
		(mDataStore->QWords).push_back( hist );
	
	}
	LOGI("Progress quantiz qwords %d" , (mDataStore->QWords).size());
	if( (mDataStore->N) > 0 ){
		updateNextState();
	}else{
		updateState(COMPLETED);
	}
}

void WLBase::toggleUseGV(){
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Setting Geo Location Sensor On");
	mDataStore->performGV = !mDataStore->performGV;
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Geo Location Sensor is On");

}

void WLBase::setGeoLocationUseOn(){
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Setting Geo Location Sensor On");
	useGeoLocation = true;
	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Geo Location Sensor is On");

}

void WLBase::setGeoLocationUseOff(){
	useGeoLocation = false;
}

void WLBase::searchFeature(){

	double idf;
	int N = mDataStore->N;
	mImagesRetrieved = new RImage[N+5];
	
	map<string, double> calculatedGeoDistance ;
	for(int i = 1 ; i <= N ; i++ ) {
		mImagesRetrieved[ i ].index  = i;
		
		double dist = -1;	
		bool isDistanceCalculated = false;				
		if(useGeoLocation){
			string ref = (mDataStore->ImageList)[ mImagesRetrieved[i].index - 1 ];
			string tempComp = ref.substr(0, ref.find_last_of("_") );
			if( calculatedGeoDistance.find(tempComp) != calculatedGeoDistance.end() ){
				dist = calculatedGeoDistance[tempComp];
				isDistanceCalculated = true;
			}
			/* First check if exist */
			if( !isDistanceCalculated  && 
				(mDataStore->LocationList).find(tempComp) != (mDataStore->LocationList).end()){
			
				__android_log_print( ANDROID_LOG_DEBUG,"Location"," Calculating distance for %d   %s %s",			mImagesRetrieved[i].index,ref.c_str(),tempComp.c_str());				
				dist = calculateGeoDistance( (mDataStore->LocationList)[tempComp] , mLastCapturedLocation );
				calculatedGeoDistance.insert(pair<string, double> (tempComp,dist));
			}
		}
		
		__android_log_print( ANDROID_LOG_DEBUG,"Distance calculate","Final Distance calcualted %0.6f ",dist);	
		
		int scoreVal = 0;
		// if dist is negative then that means we are not able to find the coordinates. So cant confirm 
		// the distance. Proceed with process
		if( dist > 100 ){
			__android_log_print( ANDROID_LOG_DEBUG,"Distance Result","Invalid target. Discarding: %0.6f",dist);	
			scoreVal = -1000;
		}
		
		mImagesRetrieved[ i ].r_score  = scoreVal;
		mImagesRetrieved[ i ].m_score  = scoreVal;
		
	}
	
	int word, count;
	for( map< int, int >::iterator it = (mDataStore->QueryHist).begin() ; it != (mDataStore->QueryHist).end() ; ++it){
		word = (*it).first;
		count = (*it).second;
		if( (mDataStore->InvertedFile)[ word ].size() == 0 )
			continue;
		
		idf = log10( N/(mDataStore->InvertedFile)[word].size() );
		if( N == 1 )	idf = 1.0;
			
		for( map< int , int >::iterator it2 = (mDataStore->InvertedFile)[word].begin() ; it2 != (mDataStore->InvertedFile)[word].end() ; ++it2 ){
			int terms = (mDataStore->TF)[ (*it2).first-1 ] > (mDataStore->QWords).size() ? (mDataStore->TF)[ (*it2).first-1 ] : (mDataStore->QWords).size() ;
			mImagesRetrieved[(*it2).first].r_score += ( ( ( count > (*it2).second ? (*it2).second : count ) * 1.0 )/terms)  * idf;
			//			mImagesRetrieved[(*it2).first].r_score += count * idf;
		}
	}
	__android_log_print( ANDROID_LOG_DEBUG,"Progress",":R Score %0.6f",mImagesRetrieved[1].r_score);

	qsort( mImagesRetrieved + 1, N , sizeof(mImagesRetrieved[ 1 ]), compareRImage );

	__android_log_print( ANDROID_LOG_DEBUG,"Progress",":R Score of %s is %0.6f",(mDataStore->ImageList)[ mImagesRetrieved[1].index - 1 ].c_str(),mImagesRetrieved[1].r_score);

	__android_log_write(ANDROID_LOG_VERBOSE,"Progress","Inverted index search");

	updateNextState();
		
}

void WLBase::doGeometricVerification(){

	if( mDataStore->performGV ){
		std::vector<KeyPoint> Rkeypoints;
		vector< int > RWords;
		vector < Mat > RMatches ;

		//	string WordKeyDir = dir + "wordkeys/wk_";
		for( int n = 1 ; n <= 5 ; n++ ){
			/*	  string RImgFilePath = WordKeyDir + ImageList[ ImagesRetrieved[n].index - 1] + ".txt";
	  ifstream RImgFile( RImgFilePath.c_str(),ios::in );
	  int countWords, Rword;
	  Point2f RPoint;
	  while( RImgFile.good() && !RImgFile.eof() ){
            RImgFile >> Rword >> RPoint.x >> RPoint.y;
	    RWords.push_back( Rword );
	    KeyPoint temp(RPoint,0);
	    Rkeypoints.push_back(temp);
	  }
	  RImgFile.close();
			 */
			Rkeypoints = (mDataStore -> keys)[ mImagesRetrieved[n].index ];
			RWords = (mDataStore ->dbWords)[ mImagesRetrieved[n].index ];
			std::vector< DMatch > imatches;
			for(int i = 0 ; i < RWords.size() ; i++ ){
				for(int j = 0 ; j < (mDataStore ->QWords).size() ; j++ ){
					if( RWords[i] == (mDataStore ->QWords)[j] ){
						//			if(matches[i] == minc[j] && fabs(distMatchQ[i] - distMatchR[i]) < 10000){
						DMatch tempDMatch;
						tempDMatch.queryIdx = i;
						tempDMatch.trainIdx = j;
						tempDMatch.distance = 0;
						imatches.push_back(tempDMatch);
						//}
					}
				}
			}
			LOGI(" initial matches: %d ", imatches.size() );
			std::vector<Point2f> obj;
			std::vector<Point2f> scene;

			for( int i = 0; i < imatches.size(); i++ )
			{
				//-- Get the keypoints from the good matches
				obj.push_back( Rkeypoints[ imatches[i].queryIdx ].pt );
				scene.push_back( keypoints[ imatches[i].trainIdx ].pt );
			}

			std::vector<uchar> inliers(obj.size(),0);
			if(obj.size() == 0 ) continue;
			Mat F = findFundamentalMat( obj, scene, inliers , FM_RANSAC, 2, 0.99 );
			std::vector<cv::Point2f>::const_iterator itPts=obj.begin();
			std::vector<uchar>::const_iterator itIn= inliers.begin();
			std::vector<Point2f> obj_ransac;
			std::vector<Point2f> scene_ransac;

			int it = 0, total = 0;
			while (itPts!=obj.end()) {
				if (*itIn){
					obj_ransac.push_back( Rkeypoints[ imatches[ total ].queryIdx ].pt );
					scene_ransac.push_back( keypoints[ imatches[ total ].trainIdx ].pt );
					++it;
				}
				++itPts;
				++itIn;
				total++;
			}
			mImagesRetrieved[ n ].m_score += 1.0 * it ;
			Rkeypoints.clear();
			RWords.clear();
		}
		qsort( mImagesRetrieved + 1, mDataStore->N , sizeof( mImagesRetrieved[ 1 ]), compareMImage );



		for(int n = 1 ;  n <= 10 ; n++){
			LOGI( "%s  %f", (mDataStore->Annotations)[ mImagesRetrieved[ n ].index - 1 ].c_str(), mImagesRetrieved[ n ].m_score );
		}
	}

	updateNextState();
}

void WLBase::createAnnotation(){
	string ref = (mDataStore->ImageList)[ mImagesRetrieved[1].index - 1 ];
	__android_log_write( ANDROID_LOG_DEBUG,"Progress","Annotating");

//	string check = ref.substr( 0, ref.find_last_of("_") - 1 );
//	int countCheck = 0;
//	__android_log_write( ANDROID_LOG_DEBUG,"Progress","Annotating");
	string result="";

	__android_log_print( ANDROID_LOG_DEBUG,"Progress",":R Score of %s is %0.6f",ref.c_str(),mImagesRetrieved[1].r_score);
	if( mDataStore->performGV && !( ( mImagesRetrieved[1].m_score - 70 >= 0 ) && (mImagesRetrieved[1].r_score - 0.035 >= 0) ) ){
		result = "NULL";
	}else if( !mDataStore->performGV && mImagesRetrieved[1].r_score - 0.035  <= 0 ){
		result = "NULL";
	}else{

		map<string, int> componentScores;
		Object compTopTen[20];
		int countCompTopTen = 0;
		for( int n = 1 ; n <= min(10, (mDataStore->N)) ; n++ ){
			__android_log_print( ANDROID_LOG_DEBUG,"Annotation","%s: %lf %lf: %s",
					(mDataStore->ImageList)[ mImagesRetrieved[n].index - 1 ].c_str(), mImagesRetrieved[n].r_score,
					mImagesRetrieved[n].m_score,
					(mDataStore->Annotations)[mImagesRetrieved[n].index -1].c_str() );

			if( n>1 ){
				double ratio_check = (mImagesRetrieved[n-1].r_score - mImagesRetrieved[n].r_score)/mImagesRetrieved[n-1].r_score;
				if( ( ratio_check - 0.50 ) > 0 ){
					break;
				}
			}
			ref = (mDataStore->ImageList)[ mImagesRetrieved[n].index - 1 ];
			string tempComp = ref.substr(0, ref.find_last_of("_") );
			if(  componentScores.count(tempComp) > 0 ){
				if(mDataStore->performGV){
					compTopTen[ componentScores[tempComp] ].score+=(mImagesRetrieved[n].m_score-20 < 0 ? 0:mImagesRetrieved[n].m_score-20);
				}else{
					compTopTen[ componentScores[tempComp] ].score+=mImagesRetrieved[n].r_score;
				}
				__android_log_print( ANDROID_LOG_DEBUG,"Annotation","Name:%s %d %f",tempComp.c_str(), componentScores[tempComp] ,compTopTen[ componentScores[tempComp] ].score );
			}else{
				countCompTopTen++;
				componentScores[tempComp] = countCompTopTen;
				compTopTen[ countCompTopTen ].name = tempComp;
				__android_log_print( ANDROID_LOG_DEBUG,"Annotation","Name:%s %d",tempComp.c_str(),countCompTopTen);
				
				if(mDataStore->performGV){
					compTopTen[ countCompTopTen ].score=(mImagesRetrieved[n].m_score-20 < 0 ? 0:mImagesRetrieved[n].m_score-20);
				}else{
					compTopTen[ countCompTopTen ].score = mImagesRetrieved[n].r_score;
				}
			}
		}
		qsort(compTopTen+1, countCompTopTen, sizeof(compTopTen[1]), compareObject);
		__android_log_write( ANDROID_LOG_DEBUG,"Progress","Annotating");


		for( int i = 1 ; i <= min(countCompTopTen,5) ; i++ ){
			if( i>1 ){
				double ratio_check = (compTopTen[i-1].score - compTopTen[i].score)/compTopTen[i-1].score;
				if( ( ratio_check - 0.50 ) > 0 ){
				  break;
				}
			}
			result = result + compTopTen[i].name + ":";
		}
		if( !mDataStore->performGV && (mImagesRetrieved[1].r_score - 0.040)  <= 0 ){
			result = result + "NULL";
		}

	}
	mFinalResult = result;
	updateNextState();
}
void WLBase :: verifyResult(){
	bool isVerified = true;
	/* Do verification here */
	if(isVerified){
		updateState(COMPLETION_SUCCESS);
	}
	else{
		updateState(COMPLETION_FAILURE);
	}
}

double WLBase::calculateGeoDistance(Location l1, Location l2){
	jclass activityClass = mJniEnv->GetObjectClass(mJniObj);
    jmethodID getCalculateDistanceMethodID = mJniEnv->GetMethodID(activityClass,
                                                    "sendGeoDistanceToNative", "(DDDD)D");
    if (getCalculateDistanceMethodID == 0)
    {
        __android_log_write(ANDROID_LOG_VERBOSE,"Distance Calculator","Function not found.");
        return -1;
    }
    double dist =  mJniEnv->CallDoubleMethod(mJniObj, getCalculateDistanceMethodID,l1.lat,l1.lon,l2.lat,l2.lon);
	__android_log_print( ANDROID_LOG_DEBUG,"Distance calculate","Distance calcualte %0.6f between %0.6f %0.6f and %0.6f %0.6f",dist,l1.lat,l1.lon,l2.lat,l2.lon);	
	return dist;
}

bool WLBase::isDirectionValidated(){
	jclass activityClass = mJniEnv->GetObjectClass(mJniObj);
    jmethodID getDirectionMethodID = mJniEnv->GetMethodID(activityClass,
                                                    "isDirectionValidated", "()Z");
    if (getDirectionMethodID == 0)
    {
        __android_log_write(ANDROID_LOG_VERBOSE,"Distance Calculator","Function not found.");
        return -1;
    }
    bool ret =  mJniEnv->CallBooleanMethod(mJniObj, getDirectionMethodID);
	__android_log_print( ANDROID_LOG_DEBUG,"Direction calculate","Direction Calculate %d",ret);	
	return ret;
}
