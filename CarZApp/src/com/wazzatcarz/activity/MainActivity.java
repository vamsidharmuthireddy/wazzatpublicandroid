package com.wazzatcarz.activity;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.widget.ProgressBar;

import com.wazzatcarz.activity.R;
import com.wazzatcarz.common.CVLoadOverListener;
import com.wazzatcarz.common.CVLoadTask;

public class MainActivity extends Activity implements CVLoadOverListener{

	public ProgressBar _progress_bar;
	CVLoadTask bgThread;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Log.i("MainActivity","method init to be called");
		init();
		Log.i("MainActivity","method init finished");
	}
	private void init() {
		// Create new directory if not already present
		File own_dir = new File(Environment.getExternalStorageDirectory()+File.separator+"wazzat");
		if(!own_dir.exists()){
			own_dir.mkdirs();
		}
		//Call necessary initial load functions
		//then go to next intent	 
		_progress_bar = (ProgressBar) findViewById(R.id.loadTaskPrgressbar);
		bgThread = new CVLoadTask(this, 0);
		bgThread.setCVLoadOverListener(this);
		bgThread.execute();
		
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		Log.i("MainActivity","Destroying");
		finish();
//		System.exit(0);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public void onDataLoaded(boolean success) {
		// TODO Auto-generated method stub
		Log.i("MainActivity","OnDataLoaded");
		if( success == true){
			Intent intent = new Intent(MainActivity.this, Start.class);
			Log.i("MainActivity","Redirecting to new activity");
			startActivity(intent);
		}
	}
	
	
}