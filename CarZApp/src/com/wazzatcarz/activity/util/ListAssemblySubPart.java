package com.wazzatcarz.activity.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import android.graphics.Color;
import android.util.Log;



public class ListAssemblySubPart{

	/**
	 * Location of image where images are stored. Should be fixed
	 */
	public final static  String FILEPATH = "/sdcard/Carz/assembly/ObjectParts_withRects.txt"  ;
	
	public final static String DELIMETER ="|";

	/**
	 * List of all the image read from the file.
	 */
	public static ArrayList<AssemblySubPartRectDetail> mSubPartData = new ArrayList<AssemblySubPartRectDetail>();

	/**
	 * Color Tolerance
	 */
	public static int mTolerance = 25;
	

	public ListAssemblySubPart(){
		
	}
	
	public  void load(){
		File file = new File(FILEPATH);

		BufferedReader br;
		String line="";
		Log.i("Checking", "Loading some shits");

		mSubPartData.clear();
		try {
			br = new BufferedReader(new FileReader(file));

			while ((line = br.readLine()) != null) {
				
				String[] parts = line.split(":");
				Log.i("Checking", line);
				Log.i("Fcked", Integer.toString(parts.length));
				if(parts.length == 3){
					Log.i("Starting", parts[0]+":"+parts[1] +":"+ parts[2]);
					AssemblySubPartRectDetail curr = new AssemblySubPartRectDetail(
							parts[0], 
							parts[1],
							parts[2]);

					mSubPartData.add(curr);

				} //end if
			} //end while
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  //end catch
	} //end function
	
	public ArrayList<AssemblySubPartRectDetail> getSubPartList(String id){
		ArrayList<AssemblySubPartRectDetail> tmp = new ArrayList<AssemblySubPartRectDetail>();
		for(AssemblySubPartRectDetail each : mSubPartData){
			if( each.getImageId().equalsIgnoreCase(id) ){
				tmp.add(each);
			}
		}
		return tmp;
	}
	
	public String getImageName(String id, int color){
		String tmp = "";
		Log.i("Checking", "Some shit is about it loader");

		for(AssemblySubPartRectDetail each : mSubPartData){
			Log.i("Checking", each.getImageId() +":"+ each.getName());
			if( each.getImageId().equalsIgnoreCase(id) ){
//				&& isColorMatch(color,each.getColor()) ){
				tmp = each.getName();
			}
		}
		return tmp;
	}
	
	public boolean isColorMatch(int color1, int color2){
		if ((int) Math.abs (Color.red (color1) - Color.red (color2)) > mTolerance ) return false;
	    if ((int) Math.abs (Color.green (color1) - Color.green (color2)) > mTolerance ) return false;
	    if ((int) Math.abs (Color.blue (color1) - Color.blue (color2)) > mTolerance ) return false;
	    return true;
	}
}
