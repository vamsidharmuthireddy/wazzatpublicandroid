package com.wazzatcarz.activity.util;
import android.graphics.Rect;
import android.os.Environment;
import android.util.Log;

public class AssemblySubPartRectDetail {
	public final  String FILEPATH = Environment.getExternalStorageDirectory().getPath() + "/Carz/assembly/"  ;

	/**
	 * ID of the sub part. Its image will be fetched from the FILEPATH
	 */
	private String mImageId="";

	/**
	 * Color of the rectangle represented by this sub part
	 */

	private Rect subPartPos;

	/**
	 * Proper name of this image
	 */
	private String mName;	


	public AssemblySubPartRectDetail(String id, String pos, String name){
		mImageId = id;
		mName = name;
		String[] posParts = pos.split("_");
		int[] x = new int[4];
		Log.i("AssemblySubPartRectDetail","posPart " + Integer.toString(posParts.length) + " " + posParts[0]);
		for( int i = 0 ; i < posParts.length ; i++ ){
			if( posParts[i].compareTo("") != 0 ){
				x[i] = Integer.parseInt(posParts[i]);
			}
		}
/*		int x1 = Integer.parseInt(posParts[0]);
		int y1 = Integer.parseInt(posParts[1]);
		int x2 = Integer.parseInt(posParts[2]);
		int y2 = Integer.parseInt(posParts[3]);*/
		subPartPos = new Rect(x[0],x[1],x[2],x[3]);
	}
	/**
	 * Following are the getter and setter of the member variable	 
	 * 
	 */
	public String getImageId() {
		return mImageId;
	}

	public void setImageId(String mImageId) {
		this.mImageId = mImageId;
	}

	public Rect getPartPos() {
		return subPartPos;
	}

/*	public void setColor(int mColor) {
		this.mColor = mColor;
	}
*/
	public String getName() {
		return mName;
	}

	public void setName(String name) {
		this.mName = name;
	}

}