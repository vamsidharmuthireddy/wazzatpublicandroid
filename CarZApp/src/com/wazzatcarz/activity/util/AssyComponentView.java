package com.wazzatcarz.activity.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import com.wazzatcarz.activity.R;
import com.wazzatcarz.cart.util.CartDetailsInfo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class AssyComponentView extends View{

	private ArrayList<AssemblySubPartRectDetail> subParts;
	public HashMap<String, Integer> partSelected;
	private Bitmap mBitmapSource;
	private Paint mRectUnSelected;
	private Paint mRectSelected;
	public HashMap<String,CheckBox> mSelectedImagesCheckbox = new HashMap<String, CheckBox>();
	float scaleX, scaleY;

	
	public AssyComponentView(Context context, ListAssemblySubPart subPartsData, String AssyId) {
		super(context);
		// TODO Auto-generated constructor stub
		String imgFilePath = Environment.getExternalStorageDirectory().getPath()+ "/Carz/assembly/" + AssyId + ".jpg";
		subParts = subPartsData.getSubPartList(AssyId);
		init(imgFilePath );
	}
	
	private void init( String imgFilePath){
        mRectUnSelected= new Paint();
        mRectUnSelected.setColor(Color.argb(60, 0, 0, 255));
        mRectUnSelected.setStyle(Paint.Style.STROKE);
        mRectUnSelected.setStrokeWidth(5); 
        
        mRectSelected= new Paint();
        mRectSelected.setColor(Color.BLUE);
        mRectSelected.setStyle(Paint.Style.STROKE);
        mRectSelected.setStrokeWidth(5); 

        partSelected = new HashMap<String, Integer>();
        for( AssemblySubPartRectDetail tmp : subParts ){
        	Log.i("AssyComponentView",tmp.getImageId() + " " + tmp.getName());
        	partSelected.put(tmp.getName(), 0);
        }
        Log.i("AssyComponentView",imgFilePath);
        mBitmapSource = BitmapFactory.decodeFile(imgFilePath);
        
	}

	
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		if( mBitmapSource != null ){
			Rect imgPos = new Rect(0,0,getWidth(),getHeight());
			canvas.drawBitmap(mBitmapSource, null, imgPos, null);
			scaleX =(1.0f* getWidth())/mBitmapSource.getWidth();
	        scaleY =(1.0f* getHeight())/mBitmapSource.getHeight();
		}
        Log.i("AssyComponentView",Integer.toString(getWidth()) + " " + Integer.toString(getHeight()) + " " + Float.toString(scaleX) + " " + Float.toString(scaleY));

		
		int i = 0;
		canvas.save();
		canvas.scale(scaleX, scaleY);
		for( AssemblySubPartRectDetail tmp : subParts ){
			if( partSelected.get(tmp.getName()) == 1 ){
				canvas.drawRect(tmp.getPartPos(), mRectSelected);
				Log.i("AssyComponentView","blue");
			}else{
				canvas.drawRect(tmp.getPartPos(), mRectUnSelected);
				Log.i("AssyComponentView","alpha blue");

			}
		}
		canvas.restore();
	}
	
	@Override
	public boolean onTouchEvent(final MotionEvent event) {
		boolean readyToLoad = false;

		final int action = event.getAction();

		final int evX = (int) event.getX();
		final int evY = (int) event.getY();

		Log.i("Mask Color" , "NOTHING IS HAPPENING");


		String return_img ="";
		switch (action) {
		case MotionEvent.ACTION_DOWN :	
			Log.i("Mask Color" , "DOWNING THE ACTION");
			Log.i("Mask Color" , "UPPING THE AVTION");
			return_img = getPartClicked(evX, evY);
			if( return_img.compareTo("") == 0 )
				return false;
			Log.i("Mask Color Return Image" , return_img);
			readyToLoad = true;
			break;

		case MotionEvent.ACTION_UP :
			break;
		default:
			readyToLoad = false;
		} // end switch


		if(readyToLoad){

			toggleSelection(return_img);
			if( CartDetailsInfo.getGLOBAL_CART().contains(return_img)){
//				removeSubPartTransparentMaskOverlay(return_img);
				CartDetailsInfo.getGLOBAL_CART().remove(return_img);
			}
			else{
				CartDetailsInfo.getGLOBAL_CART().add(return_img);
//				displayTransparentSubPartMaskOverlay(return_img);

				Log.i("Mask Color" , " Loading some shit which I dont know about");
			}
		}
		return readyToLoad;
	}
	
	public void toggleSelection( String return_img ){
		if( partSelected.get(return_img) == 1 ){
			partSelected.put(return_img, 0);
			mSelectedImagesCheckbox.get(return_img).setChecked(false);
		}else{
			partSelected.put(return_img, 1);	
			mSelectedImagesCheckbox.get(return_img).setChecked(true);
		}
		invalidate();
	}
	
	private String getPartClicked(int x, int y){
		String return_img="";
		for( AssemblySubPartRectDetail tmp : subParts ){
			int sx = Math.round(x/scaleX);
			int sy = Math.round(y/scaleY);
			if( tmp.getPartPos().contains(sx,sy) ){
				return tmp.getName();
			}
		}
		return return_img;
	}
/*
	private void removeSubPartTransparentMaskOverlay(String return_img) {
		ImageView to_be_removed  = mSelectedImagesView.get(return_img);
		if(to_be_removed != null ){
			FrameLayout holder = (FrameLayout) findViewById(R.id.check_list_holder);
			holder.removeView(to_be_removed);
			CartDetailsInfo.getGLOBAL_CART().remove(return_img);
			if( mSelectedImagesCheckbox.get(return_img)!= null ){
				mSelectedImagesCheckbox.get(return_img).setChecked(false);
			}
		}
	}

	private void displayTransparentSubPartMaskOverlay(String return_img) {
		// Overlay image over background
		ImageView transparentbg_mask = new ImageView(this);
		transparentbg_mask.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		String x = return_img.replace(" ", "_");
		transparentbg_mask.setImageBitmap(BitmapFactory.decodeFile(Environment.getExternalStorageDirectory().getPath() + 
				"/Carz/assembly/" +  x + ".png"));
		CartDetailsInfo.getGLOBAL_CART().add(return_img);

		FrameLayout holder = (FrameLayout) findViewById(R.id.check_list_holder);
		holder.addView(transparentbg_mask);
		mSelectedImagesView.put(return_img, transparentbg_mask);
		if( mSelectedImagesCheckbox.get(return_img)!= null ){
			mSelectedImagesCheckbox.get(return_img).setChecked(true);
		}
	}
	
	public int getMaskColor ( int x, int y) {
		ImageView img = (ImageView) findViewById (R.id.sub_parts_rec);		
		img.setDrawingCacheEnabled(true); 
		Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache()); 
		if (hotspots == null) {
			Log.d ("ImageAreasActivity", "Hot spot bitmap was not created");
			return 0;
		} else {
			img.setDrawingCacheEnabled(false);
			return hotspots.getPixel(x, y);
		}
	}
*/


}
