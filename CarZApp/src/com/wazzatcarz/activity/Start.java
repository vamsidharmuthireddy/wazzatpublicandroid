package com.wazzatcarz.activity;

import com.wazzatcarz.cart.activity.CartCheckoutActivity;
import com.wazzatcarz.cart.activity.CartViewActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.ClipData.Item;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class Start extends Activity {
	final Context context = this;
	boolean showHelp = false;
	public static boolean GPS_ENABLED = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);



		Button query_btn =  (Button) findViewById(R.id.query);
		query_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Start.this, Sample3Native.class);
				startActivityForResult(intent,0);
			}
		});

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		finish();
		//		System.exit(1);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
	}

/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.start, menu);
		return true;
	}
*/	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.sub_part_display , menu);
		
		
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.checkout:
			Intent i1 = new Intent(Start.this, CartViewActivity.class);
			startActivityForResult(i1,0);
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}


}
