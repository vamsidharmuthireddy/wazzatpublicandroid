package com.wazzatcarz.activity;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.wazzatcarz.activity.util.AssyComponentView;
import com.wazzatcarz.activity.util.ListAssemblySubPart;
import com.wazzatcarz.cart.activity.CartCheckoutActivity;
import com.wazzatcarz.cart.activity.CartViewActivity;
import com.wazzatcarz.cart.util.CartDetailsInfo;

public class SubPartDisplayCanvasActivity extends Activity{	


	/**
	 * The instance of selected assembly which is provided from previous assembly.
	 */
	public String mSelectedAssembly= "BUMPER_FRONT_ASSY";

	/**
	 * Contains information about the all the mask.
	 */
	public ListAssemblySubPart mSubPartsLists;


	/**
	 * Map of selected part and their corresponding imageview and checkbox
	 */

	public HashMap<String,ImageView> mSelectedImagesView = new HashMap<String, ImageView>();

	/**
	 * Customized Canvas view to show a generic image of the assy component with its sub-parts highlighted
	 */
	private AssyComponentView mAssyComponentView;

	/**
	 * This list contains all the sub parts for the checkbox
	 */
	private String[] mCheckboxSubPartList ={"Bonnet","Logo","Side-logo","HeadLights","FogLights"};
	LinearLayout holder;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_sub_part_display);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mSelectedAssembly = extras.getString("PartName");
			mCheckboxSubPartList  = extras.getStringArray("SubPartName");
		}
		Log.i("SubPartDisplayActivity",Integer.toString(mCheckboxSubPartList.length));
		loadImageDetailsFromFile();
//		drawBackground(0);
		


		FrameLayout imgDisplayLayout = (FrameLayout)findViewById(R.id.check_list_holder);
		mAssyComponentView = new AssyComponentView(this, mSubPartsLists, mSelectedAssembly);
		mAssyComponentView.setLayoutParams(new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT));
		imgDisplayLayout.addView(mAssyComponentView);

		displayCheckbox();

	}

	private void displayCheckbox() {
		holder = (LinearLayout) findViewById(R.id.list_subparts_ll);
		for( final String cur_item : mCheckboxSubPartList){
			final CheckBox cb =  new CheckBox(this);
			cb.setText(cur_item);
			cb.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
			//cb.setTag(cur_item);
			cb.setTextColor(Color.BLACK);
			cb.setGravity(Gravity.CENTER | Gravity.RIGHT);
		    cb.setChecked(false);
		    cb.setBackgroundColor(Color.WHITE);
		    mAssyComponentView.mSelectedImagesCheckbox.put(cur_item, cb);
		    final float scale = this.getResources().getDisplayMetrics().density;
		    cb.setPadding(cb.getPaddingLeft() + (int)(10.0f * scale + 0.5f),
		            cb.getPaddingTop(),
		            cb.getPaddingRight(),
		            cb.getPaddingBottom());
		    cb.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					boolean isChecked = cb.isChecked();
					mAssyComponentView.toggleSelection(cur_item);
					if( isChecked){
	//					displayTransparentSubPartMaskOverlay(cur_item);
						CartDetailsInfo.getGLOBAL_CART().add(cur_item);
					}
					else{
	//					removeSubPartTransparentMaskOverlay(cur_item);	
						CartDetailsInfo.getGLOBAL_CART().remove(cur_item);
					}
					
				}
			}); 
		    
		    holder.addView(cb);
		}
		Button doneButton = new Button(this);
		doneButton.setText("DONE");
		doneButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		holder.addView(doneButton);
	}

	private void loadImageDetailsFromFile() {
		mSubPartsLists = new ListAssemblySubPart();
		mSubPartsLists.load();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.sub_part_display , menu);	
		
		return super.onCreateOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.checkout:
			Intent i1 = new Intent(SubPartDisplayCanvasActivity.this, CartViewActivity.class);
			startActivity(i1);
			return true;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onDestroy() {
		FrameLayout holder = (FrameLayout) findViewById(R.id.check_list_holder);
		holder.removeAllViews();
		super.onDestroy();
	}
	
}

