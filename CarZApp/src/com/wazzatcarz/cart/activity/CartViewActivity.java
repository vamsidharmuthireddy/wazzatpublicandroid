package com.wazzatcarz.cart.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.wazzatcarz.activity.R;
import com.wazzatcarz.activity.R.color;
import com.wazzatcarz.cart.util.CartDetailsInfo;

public class CartViewActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cart_view);
		
		populateList();
		createButton();
	}

	private void createButton() {
		Button checkout_btn =  (Button)findViewById(R.id.go_to_checkout);
		checkout_btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(CartViewActivity.this, CartCheckoutActivity.class);
				startActivityForResult(intent,0);
			}
		});
	} 

	private void populateList() {
		final LinearLayout holder = (LinearLayout) findViewById(R.id.cart_holder);
		
		
		for( String cur_item : CartDetailsInfo.getGLOBAL_CART()){
			CheckBox cb =  new CheckBox(this);
			cb.setText(cur_item);
			cb.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT));
			//cb.setTag(cur_item);
			cb.setTextColor(Color.LTGRAY);
			cb.setGravity(Gravity.CENTER | Gravity.RIGHT);
		    cb.setChecked(true);
		    cb.setBackgroundColor(color.black_overlay);
		    final float scale = this.getResources().getDisplayMetrics().density;
		    cb.setPadding(cb.getPaddingLeft() + (int)(10.0f * scale + 0.5f),
		            cb.getPaddingTop(),
		            cb.getPaddingRight(),
		            cb.getPaddingBottom());
		    cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
					if( isChecked){
						CartDetailsInfo.getGLOBAL_CART().add(buttonView.getText().toString());
					}
					else{
						CartDetailsInfo.getGLOBAL_CART().remove(buttonView.getText().toString());
					}				
				}
			});
		    
		    holder.addView(cb);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cart_view, menu);
		return true;
	}

}
